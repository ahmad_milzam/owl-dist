<!-- page footer -->
<div class="page-foot">
  <ul class="page-foot__sosmed-list">
    <?php if (!empty($site_settings['site_facebook_link'])): ?>
    <li class="page-foot__sosmed-item">
      <a href="<?php echo $site_settings['site_facebook_link']; ?>" class="page-foot__sosmed-link">
        <svg class="icon icon--facebook page-foot__sosmed-icon"><use xlink:href="#icon--facebook"></use></svg>
      </a>
    </li>
    <?php endif ?>
    <?php if (!empty($site_settings['site_twitter_link'])): ?>
    <li class="page-foot__sosmed-item">
      <a href="<?php echo $site_settings['site_twitter_link']; ?>" class="page-foot__sosmed-link">
        <svg class="icon icon--twitter page-foot__sosmed-icon"><use xlink:href="#icon--twitter"></use></svg>
      </a>
    </li>
    <?php endif ?>
    <?php if (!empty($site_settings['site_instagram_link'])): ?>
    <li class="page-foot__sosmed-item">
      <a href="<?php echo $site_settings['site_instagram_link']; ?>" class="page-foot__sosmed-link">
        <svg class="icon icon--instagram page-foot__sosmed-icon"><use xlink:href="#icon--instagram"></use></svg>
      </a>
    </li>
    <?php endif ?>
    <?php if (!empty($site_settings['site_gplus_link'])): ?>
    <li class="page-foot__sosmed-item">
      <a href="<?php echo $site_settings['site_gplus_link']; ?>" class="page-foot__sosmed-link">
        <svg class="icon icon--google page-foot__sosmed-icon"><use xlink:href="#icon--google"></use></svg>
      </a>
    </li>
    <?php endif ?>
    <?php if (!empty($site_settings['site_youtube_link'])): ?>
      <li class="page-foot__sosmed-item">
        <a href="<?php echo $site_settings['site_youtube_link']; ?>" class="page-foot__sosmed-link">
          <svg class="icon icon--youtube page-foot__sosmed-icon"><use xlink:href="#icon--youtube"></use></svg>
        </a>
      </li>
    <?php endif ?>
  </ul>
  <div class="page-foot__contact">
    <div><?php echo $site_settings['site_company_name']; ?></div>
    <div><?php echo $site_settings['site_address']; ?></div>
    <div>Telp: <?php echo $site_settings['site_phone']; ?></div>
    <div>Hp: <?php echo $site_settings['site_mobile_phone']; ?></div>
    <div>Email: <?php echo $site_settings['site_email']; ?></div>
  </div>
  <div class="page-foot__detail">
    <div class="page-foot__copyright page-foot__copyright--left">
      copyrights@owl-plantation.com
    </div>
    <div class="page-foot__copyright page-foot__copyright--right">
      <a href="http://www.jakartaweb.com/" target="_blank" class="link-dark">Web Design &amp; Development by Jakartaweb</a>
    </div>
  </div>
</div>
<!-- page footer -->