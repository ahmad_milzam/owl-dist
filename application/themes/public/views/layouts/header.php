<!-- page head -->
<div class="page-head">
  <div class="page-head__container">
    <!-- site navigation -->
    <nav class="site-nav">
      <!-- logo -->
      <a href="" class="site-nav__logo">
        <img class="site-nav__logo-img" src="<?php echo config_item('assets_url'); ?>front/img/logo.png" alt="OWL Plantation"/>
      </a>
      <!-- logo -->

      <button type="button" class="site-nav__toggle burger" id="js-navToggle">
        <span class="burger__bar"></span>
      </button>

      <!-- navigation link -->
      <ul class="site-nav__list" id="js-navList">
        <li class="site-nav__item">
          <a class="site-nav__link <?php echo $this->uri->segment(1) == "" ? 'is-active' : ''; ?>" href="<?php echo base_url(); ?>">Home</a>
        </li>
        <li class="site-nav__item">
          <a class="site-nav__link <?php echo $this->uri->segment(1) == "about" ? 'is-active' : ''; ?>" href="<?php echo base_url('about'); ?>">About OWL</a>
        </li>
        <li class="site-nav__item">
          <a class="site-nav__link <?php echo $this->uri->segment(1) == "testimonials" ? 'is-active' : ''; ?>" href="<?php echo base_url('testimonials'); ?>">Testimonials</a>
        </li>
        <li class="site-nav__item">
          <a class="site-nav__link <?php echo $this->uri->segment(1) == "why" ? 'is-active' : ''; ?>" href="<?php echo base_url('why'); ?>">Why OWL</a>
        </li>
        <li class="site-nav__item">
          <a class="site-nav__link <?php echo $this->uri->segment(1) == "news" ? 'is-active' : ''; ?>" href="<?php echo base_url('news'); ?>">News</a>
        </li>
        <li class="site-nav__item">
          <a class="site-nav__link <?php echo $this->uri->segment(1) == "contact" ? 'is-active' : ''; ?>" href="<?php echo base_url('contact'); ?>">Contact</a>
        </li>
        <li class="site-nav__item">
          <a class="site-nav__link site-nav__link--external" href="http://demo.owl-plantation.com/owl/" target="_blank">
            <span class="badge badge--inline">Live Demo</span>
          </a>
        </li>
      </ul>
      <!-- navigation link -->
    </nav><!-- site navigation -->
  </div>
</div><!-- page head -->