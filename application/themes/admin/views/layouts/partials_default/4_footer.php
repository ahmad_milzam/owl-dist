<!-- Main Footer -->
<footer class="main-footer">
	<!-- To the right -->
	<div class="pull-right hidden-xs">
		Page rendered in <strong>{elapsed_time}</strong> seconds, using <strong>{memory_usage}</strong>.
	</div>
	<!-- Default to the left -->
	&nbsp;
</footer>