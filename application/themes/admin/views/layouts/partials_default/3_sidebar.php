<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">GENERAL NAVIGATION</li>
            <li class="<?php echo $this->uri->segment(2) == "" ? 'active' : ''; ?>">
                <a href="<?php echo base_url('admin'); ?>">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>

            <li class="<?php echo $this->uri->segment(2) == "banners" ? 'active' : ''; ?>">
                <a href="<?php echo base_url('admin/banners'); ?>">
                    <i class="fa fa-picture-o"></i> <span>Banners</span>
                </a>
            </li>

            <li class="<?php echo $this->uri->segment(2) == "files" ? 'active' : ''; ?>">
                <a href="<?php echo base_url('admin/files'); ?>">
                    <i class="fa fa-file"></i> <span>Files</span>
                </a>
            </li>

            <li class="<?php echo $this->uri->segment(2) == "news" ? 'active' : ''; ?>">
                <a href="<?php echo base_url('admin/news'); ?>">
                    <i class="fa fa-file-text"></i> <span>News</span>
                </a>
            </li>

            <li class="<?php echo $this->uri->segment(2) == "testimonials" ? 'active' : ''; ?>">
                <a href="<?php echo base_url('admin/testimonials'); ?>">
                    <i class="fa fa-youtube-play"></i> <span>Testimonials</span>
                </a>
            </li>

            <li class="<?php echo $this->uri->segment(2) == "file_manager" ? 'active' : ''; ?>">
                <a href="<?php echo base_url('admin/file_manager'); ?>">
                    <i class="fa fa-folder-open"></i> <span>File Manager</span>
                </a>
            </li>

            <li class="header">ADMIN NAVIGATION</li>
            <!-- Menu only for admin -->
            <?php if($this->ion_auth->is_admin()) : ?>
            <!--<li class="treeview <?php echo $this->uri->segment(2) == "users" && ! in_array($this->uri->segment(3), array('profile')) || $this->uri->segment(2) == "groups" ? 'active' : ''; ?>">
                <a href="#">
                    <i class='fa fa-users'></i> <span>User Management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo $this->uri->segment(2) == "users" && ! in_array($this->uri->segment(3), array('groups','profile')) ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('admin/users'); ?>">
                            <i class="fa fa-circle-o"></i> Users
                        </a>
                    </li>
                    <li class="<?php echo $this->uri->segment(2) == "groups" ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('admin/groups'); ?>">
                            <i class="fa fa-circle-o"></i> Groups
                        </a>
                    </li>
                </ul>
            </li>-->
            <li class="<?php echo $this->uri->segment(2) == "users" ? 'active' : ''; ?>">
                <a href="<?php echo base_url('admin/users'); ?>">
                    <i class="fa fa-users"></i> <span>Users</span>
                </a>
            </li>
            <?php endif; ?>
            <li class="<?php echo $this->uri->segment(2) == "settings" ? 'active' : ''; ?>">
                <a href="<?php echo base_url('admin/settings'); ?>">
                    <i class="fa fa-wrench"></i> <span>Settings</span>
                </a>
            </li>

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>