<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Admin_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	//--------------------------------------------------------------------


	public function index()
	{
        $this->load->model('news/news_model');
        $this->load->model('files/files_model');
        $this->load->model('banners/banners_model');

        $this->lang->load('banners/banners');
        $this->lang->load('files/files');
        $this->lang->load('news/news');

        $latest_news 		= $this->news_model->order_by('created_on', 'desc')->limit(1)->find_all_published();
        $latest_files 		= $this->files_model->order_by('created_on', 'desc')->limit(1)->find_all_published();
        $latest_banners 	= $this->banners_model->order_by('created_on', 'desc')->limit(1)->find_all_published();

		/// Breadcrumb setting
	    $this->load->library('breadcrumb');

	    $this->breadcrumb->home_title('<i class="fa fa-home"></i> Home', 'admin');
	    $this->breadcrumb->add('Dashboard');
        // End Breadcrumb setting

	    $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);

	    $vars = array(
	    				'breadcrumb' 		=> $breadcrumb,
	    				'latest_news' 		=> $latest_news,
	    				'latest_files' 		=> $latest_files,
	    				'latest_banners' 	=> $latest_banners,
	    			);

	    // print_r($vars);

	    $this->template->set($vars);
		$this->template->title('Dashboard');
		$this->render('dashboard_view');
	}

	//--------------------------------------------------------------------
}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */