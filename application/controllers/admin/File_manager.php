<?php defined('BASEPATH') OR exit('No direct script access allowed');

class File_manager extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    //--------------------------------------------------------------------


    public function index()
    {
        // Breadcrumb setting
        $this->load->library('breadcrumb');

        $this->breadcrumb->home_title('<i class="fa fa-home"></i> Home', 'admin');
        $this->breadcrumb->add('File Manager');
        // End Breadcrumb setting

        $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);

        $vars = array(
                        'breadcrumb'    => $breadcrumb
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('File Manager');
        $this->render('file_manager_view');
    }

    //--------------------------------------------------------------------
}