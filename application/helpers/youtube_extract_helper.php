<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function youtube_extract($url)
{
//	$link=substr($link,strpos($link,'=')+1);
//	if(!empty($link))
//	{
//	return $link;
//	}
    
    $protocol = '(http://)|(http://www.)|(www.)';
	$protocol = str_replace('.', '\.', str_replace('/', '\/', $protocol)); 
	$protocol = ($protocol != '') ? '^(' . $protocol . ')' : $protocol; 
    $match_str = '/' . $protocol . 'youtube\.com\/(.+)(v=.+)/'; 

	preg_match($match_str, $url, $matches); 

	if($matches != null)
	{
		if(count($matches) >= 3)
		{
			$qs = explode('&',$matches[count($matches)-1]); 
			$vid = false; 
			for($i=0; $i<count($qs); $i++)
			{
				$x = explode('=', $qs[$i]); 
				if($x[0] == 'v' && $x[1])
				{ 
					$vid = $x[1]; 
					return $vid;
				}
				else
				{
					return false;
				}
			}
		}
	}
}

function getYouTubeIdFromURL($url) 
{
  $pattern = '/(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i';
  preg_match($pattern, $url, $matches);

  return isset($matches[1]) ? $matches[1] : false;
}