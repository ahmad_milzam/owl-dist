<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function generate_slug($string)
{
    $str  = array('"', '<', '>', '!', '(', ')', '?', '~', '/', '|', ':', ';', '{', '}', '-', '+', '_', '%', '&', '*', '^', '#', '@', '$', '=');
    $slug = str_replace($str, "", $string);
    $slug = str_replace(" ", "-", $slug);

    return strtolower($slug);
}