<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Breadcrumb
{
    private $_include_home      = 'Home';
    private $_breadcrumb        = array();
    private $_divider           = '';
    private $_container_open    = '<div id="breadcrumb">';
    private $_container_close   = '</div>';
    private $_tag_open          = '';
    private $_tag_close         = '';
    private $_last_tag_open     = '';
    private $_last_tag_close    = '';


    public function __construct()
    {
        $CI =& get_instance();
        $CI->load->helper('url');
    }

    public function home_title($home_title = NULL, $href = '')
    {
        if($href == "")
        {
            $href = rtrim(base_url(),'/');
        }
        else
        {
            $href = site_url($href);
        }

        if(isset($home_title) && (sizeof($home_title) > 0))
        {
            $this->_breadcrumb[] = array('title'=>$home_title, 'href'=>$href);
        }
        else
        {
            $this->_breadcrumb[] = array('title'=>$this->_include_home, 'href'=>$href);
        }
    }

    public function add($title = NULL, $href = '', $segment = FALSE)
    {
        // if the method won't receive the $title parameter, it won't do anything to the $_breadcrumb
        if (is_null($title)) return;
        // first let's find out if we have a $href
        if(isset($href) && strlen($href)>0)
        {
            // if $segment is not FALSE we will build the URL from the previous crumb
            if ($segment)
            {
                $previous = $this->_breadcrumb[sizeof($this->_breadcrumb) - 1]['href'];
                $href = $previous . '/' . $href;
            }
            // else if the $href is not an absolute path we compose the URL from our site's URL
            elseif (!filter_var($href, FILTER_VALIDATE_URL))
            {
                $href = site_url($href);
            }
        }
        // add crumb to the end of the breadcrumb
        $this->_breadcrumb[] = array('title' => $title, 'href' => $href);
    }

    public function output($param = NULL) //param = container_open, tag_open, tag_close, divider, container_close
    {
        // we open the container's tag
        $output = ! empty($param['container_open']) ? $param['container_open'] : $this->_container_open;
        if(sizeof($this->_breadcrumb) > 0)
        {
            $i = 0;
            foreach($this->_breadcrumb as $key=>$crumb)
            {
                // we put the crumb with open and closing tags
                // use the last tag for the last crumb
                if($key == sizeof($this->_breadcrumb)-1)
                {
                    $output .= ! empty($param['last_tag_open']) ? $param['last_tag_open'] : $this->_last_tag_open;
                }
                else
                {
                    $output .= ! empty($param['tag_open']) ? $param['tag_open'] : $this->_tag_open;
                }

                if(strlen($crumb['href'])>0)
                {
                    $output .= anchor($crumb['href'],$crumb['title']);
                }
                else
                {
                    $output .= $crumb['title'];
                }

                // use the last tag for the last crumb
                if($key == sizeof($this->_breadcrumb)-1)
                {
                    $output .= ! empty($param['last_tag_close']) ? $param['last_tag_close'] : $this->_last_tag_close;
                }
                else
                {
                    $output .= ! empty($param['tag_close']) ? $param['tag_close'] : $this->_tag_close;
                }

                // we end the crumb with the divider if is not the last crumb
                if($key < (sizeof($this->_breadcrumb)-1))
                {
                    $output .= ! empty($param['divider']) ? $param['divider'] : $this->_divider;
                }

                $i++;
            }
        }
        // we close the container's tag
        $output .= ! empty($param['container_close']) ? $param['container_close'] : $this->_container_close;
        return $output;
    }
}