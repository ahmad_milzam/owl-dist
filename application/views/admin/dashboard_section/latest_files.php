<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title">Latest Files</h3>
		</div><!-- /.box-header -->
		<div class="box-body table-responsive no-padding">
			<table class="table table-hover">
				<tbody>
					<tr>
						<th>Title</th>
						<th>File</th>
						<th>Created</th>
						<th>Action</th>
					</tr>
				<?php if(! empty($latest_files)) : ?>
					<?php foreach($latest_files AS $files) : ?>
					<tr>
						<td><?php echo $files->title; ?></td>
						<td>
							<a href="<?php echo config_item('uploads_url').$files->file; ?>" target="_blank">
								<?php
									$file = explode('/', $files->file);
									echo $file[1];
								?>
							</a>
						</td>
						<td><?php echo date('d F Y - H:i',strtotime($files->created_on));?></td>
						<td>
							<?php
	    						echo anchor(SITE_AREA.'/'.lang('files.slug').'/edit/' . $files->id,
	    												'<i class="fa fa-fw fa-edit"></i> '.lang('files.edit_button'),
	    												array('class' => 'btn btn-warning btn-sm', 'title' => lang('files.edit_caption'))
											);
	    					?>
	    					<?php
								echo anchor(SITE_AREA.'/'.lang('files.slug').'/move_trash/' . $files->id,
														'<i class="fa fa-fw fa-trash-o"></i> '.lang('files.trash_button'),
														array('class' => 'btn btn-danger btn-sm', 'title' => lang('files.trash_caption'), "onclick" => "return confirm('".lang("files.trash_confirm")."')")
	                                        );
	    					?>
						</td>
					</tr>
					<?php endforeach; ?>
				<?php else : ?>
					<tr>
						<td colspan="5">Empty records..</td>
					</tr>
				<?php endif; ?>
				</tbody>
			</table>
		</div><!-- /.box-body -->
		<div class="box-footer text-center">
			<?php
				echo anchor(SITE_AREA.'/'.lang('files.slug'),
										'View All Files',
										array('class' => 'uppercase')
							);
			?>
		</div>
	</div><!-- /.box -->
</div>