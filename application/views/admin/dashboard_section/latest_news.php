<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title">Latest News</h3>
		</div><!-- /.box-header -->
		<div class="box-body table-responsive no-padding">
			<table class="table table-hover">
				<tbody>
					<tr>
						<th>Image</th>
						<th>Title</th>
						<th>Summary</th>
						<th>Created</th>
						<th>Action</th>
					</tr>
				<?php if(! empty($latest_news)) : ?>
					<?php foreach($latest_news AS $news) : ?>
					<tr>
						<td>
							<img src="<?php echo config_item('uploads_url').$news->image; ?>" width="150px">
						</td>
						<td><?php echo $news->title; ?></td>
						<td><?php echo $news->summary; ?></td>
						<td><?php echo date('d F Y - H:i',strtotime($news->created_on));?></td>
						<td>
							<?php
	    						echo anchor(SITE_AREA.'/'.lang('news.slug').'/edit/' . $news->id,
	    												'<i class="fa fa-fw fa-edit"></i> '.lang('news.edit_button'),
	    												array('class' => 'btn btn-warning btn-sm', 'title' => lang('news.edit_caption'))
											);
	    					?>

	    					<?php
								echo anchor(SITE_AREA.'/'.lang('news.slug').'/move_trash/' . $news->id,
														'<i class="fa fa-fw fa-trash-o"></i> '.lang('news.trash_button'),
														array('class' => 'btn btn-danger btn-sm', 'title' => lang('news.trash_caption'), "onclick" => "return confirm('".lang("news.trash_confirm")."')")
	                                        );
	    					?>
						</td>
					</tr>
					<?php endforeach; ?>
				<?php else : ?>
					<tr>
						<td colspan="5">Empty records..</td>
					</tr>
				<?php endif; ?>
				</tbody>
			</table>
		</div><!-- /.box-body -->
		<div class="box-footer text-center">
			<?php
				echo anchor(SITE_AREA.'/'.lang('news.slug'),
										'View All News',
										array('class' => 'uppercase')
							);
			?>
		</div>
	</div><!-- /.box -->
</div>