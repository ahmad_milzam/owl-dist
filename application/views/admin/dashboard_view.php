<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
		Dashboard
		</h1>
		<?php echo $breadcrumb; ?>
	</section>

	<!-- Main content -->
	<section class="content">

		<?php if($this->session->flashdata('access_error_message') != "") : ?>
	    <div class="alert alert-danger alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	        <h4><i class="icon fa fa-ban"></i> Access Denied!</h4>
	        <?php echo $this->session->flashdata('access_error_message'); ?>
	    </div>
	    <?php endif; ?>

	    <div class="row">
	    	<!-- Latest News -->
	    	<?php include('dashboard_section/latest_news.php'); ?>
	    	<!-- End Latest News -->

	    	<!-- Latest Files -->
	    	<?php include('dashboard_section/latest_files.php'); ?>
	    	<!-- End Latest Files -->
	    </div>

	</section><!-- /.content -->

</div><!-- /.content-wrapper -->