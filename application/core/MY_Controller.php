<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * Base Controller
 *
**/
class MY_Controller extends MX_Controller
{
    protected $_carabiner;
    protected $data     = array();
    protected $main_css    = array();
    protected $main_js     = array();
    protected $local_css   = array();
    protected $local_js    = array();

    function __construct()
    {
        parent::__construct();

        // Load settings library
        $this->load->library('settings/settings_lib');

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Pragma: no-cache");
    }

    protected function _carabiner($theme)
    {
        // Load carabiner asset management library
        $this->load->library('carabiner');

        // Setting carabiner config
        $carabiner_config = array(
                                'script_dir'    => 'assets/'. $theme .'/',
                                'style_dir'     => 'assets/'. $theme .'/',
                                'cache_dir'     => 'assets/'. $theme .'/cache/',
                                'combine'       => FALSE,
                                'base_uri'      => base_url(),
                                // 'minify_css'    => ENVIRONMENT == 'development'? FALSE : TRUE,
                                // 'minify_js'     => ENVIRONMENT == 'development'? FALSE : TRUE,
                                'minify_css'    => FALSE,
                                'minify_js'     => FALSE,
                            );

        // Initialize carabiner library with config
        $this->carabiner->config($carabiner_config);
    }

    protected function render($view, $layout)
    {
        if($layout == 'json' || $this->input->is_ajax_request())
        {
            header('Content-Type: application/json');
            echo json_encode($this->data);
        }
        else
        {
            $this->template->set_layout($layout);
            $this->template->build($view);
        }
    }
}

//--------------------------------------------------------------------


/**
 *
 * Authenticated Controller
 *
**/
class Authenticated_Controller extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        // Initialize Carabiner
        $this->_carabiner('admin');
    }

    protected function render($view, $layout = 'login')
    {
        // Load main assets for login theme
        $this->main_css = array(
                                array('bootstrap/css/bootstrap.min.css'),
                                array('plugins/font-awesome/css/font-awesome.min.css'),
                                array('plugins/ionicons/css/ionicons.min.css'),
                                array('css/AdminLTE.min.css'),
                                array('plugins/icheck/square/_all.css'),
                            );
        $this->main_js = array(
                                array('plugins/jquery/jquery-2.1.4.min.js'),
                                array('bootstrap/js/bootstrap.min.js'),
                                array('plugins/icheck/icheck.min.js'),
                            );
        // Compile all main assets
        $this->carabiner->group('main_css', array('css'=>$this->main_css) );
        $this->carabiner->group('main_js', array('js'=>$this->main_js) );

        $this->template->set_theme('admin');
        parent::render('admin/'.$view, $layout);
    }
}

//--------------------------------------------------------------------


/**
 *
 * Admin Controller
 *
**/
class Admin_Controller extends MY_Controller
{
    protected $current_user;
    protected $breadcrumb_param;
    protected $pager_param;

    function __construct()
    {
        parent::__construct();

        $this->load->library('users/ion_auth');
        if (!$this->ion_auth->logged_in())
        {
            //redirect them to the login page
            $this->session->set_flashdata('message','You must be logged in to view that page.');
            redirect('login', 'refresh');
        }

        $this->data_limit = $this->settings_lib->item('site_page_limit');

        $this->current_user = $this->ion_auth->user()->row();

        $this->breadcrumb_param = array(
                                        'container_open'    => '<ol class="breadcrumb">',
                                        'container_close'   => '</ol>',
                                        'tag_open'          => '<li>',
                                        'tag_close'         => '</li>',
                                        'last_tag_open'     => '<li class="active">',
                                        'last_tag_close'    => '</li>',
                                    );

        $this->pager_param = array(
                                        'full_tag_open'     => '<div class="box-footer clearfix"><ul class="pagination pagination-sm no-margin pull-right">',
                                        'full_tag_close'    => '</ul></div>',
                                        'first_tag_open'    => '<li>',
                                        'first_link'        => '«',
                                        'first_tag_close'   => '</li>',
                                        'last_tag_open'     => '<li>',
                                        'last_link'         => '»',
                                        'last_tag_close'    => '</li>',
                                        'prev_tag_open'     => '<li>',
                                        'prev_link'         => '&lt;',
                                        'prev_tag_close'    => '</li>',
                                        'next_tag_open'     => '<li>',
                                        'next_link'         => '&gt;',
                                        'next_tag_close'    => '</li>',
                                        'cur_tag_open'      => '<li class="active"><a href="#">',
                                        'cur_tag_close'     => '</a></li>',
                                        'num_tag_open'      => '<li>',
                                        'num_tag_close'     => '</li>',
                                    );

        // Initialize Carabiner
        $this->_carabiner('admin');
    }

    protected function render($view, $layout = 'default')
    {
        // Load main assets for admin theme
        $this->main_css = array(
                                array('css/admin.css'),
                                array('bootstrap/css/bootstrap.min.css'),
                                array('bootstrap/css/bootstrap-datepicker3.min.css'),
                                array('plugins/font-awesome/css/font-awesome.min.css'),
                                array('plugins/ionicons/css/ionicons.min.css'),
                                array('plugins/fancybox/jquery.fancybox.css'),
                                array('plugins/fancybox/helpers/jquery.fancybox-buttons.css'),
                                array('css/AdminLTE.min.css'),
                                array('css/skins/skin-blue.min.css'),
                            );
        $this->main_js = array(
                                array('plugins/jquery/jquery-2.1.4.min.js'),
                                array('plugins/jquery/jquery-ui.min.js'),
                                array('bootstrap/js/bootstrap.min.js'),
                                array('bootstrap/js/bootstrap-datepicker.min.js'),
                                array('plugins/fastclick/fastclick.min.js'),
                                array('plugins/slimscroll/jquery.slimscroll.min.js'),
                                array('plugins/fancybox/jquery.fancybox.pack.js'),
                                array('plugins/fancybox/helpers/jquery.fancybox-buttons.js'),
                                array('plugins/fancybox/helpers/jquery.fancybox-media.js'),
                                array('plugins/ckeditor/ckeditor.js'),
                                array('js/app.min.js'),
                                array('js/admin.js')
                            );
        // Compile all main assets
        $this->carabiner->group('main_css', array('css'=>$this->main_css) );
        $this->carabiner->group('main_js', array('js'=>$this->main_js) );

        $this->template->set_theme('admin');
        parent::render('admin/'.$view, $layout);
    }
}

//--------------------------------------------------------------------


/**
 *
 * Public Controller
 *
**/
class Public_Controller extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->site_settings = $this->settings_lib->find_all();
    }

    protected function render($view, $layout = 'default')
    {
        $vars = array(
                        'site_settings' => $this->site_settings
                    );

        $this->template->set($vars);

        if($this->site_settings['site_status'] == 0) //offline
        {
            $this->template->set_theme('offline');
            parent::render('offline/index', 'default');
        }
        else //online
        {
            $this->template->set_theme('public');
            parent::render('public/'.$view, $layout);
        }
    }
}

//--------------------------------------------------------------------