<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends Public_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('news_model');
        $this->lang->load('news');
    }

    //--------------------------------------------------------------------


    public function index()
    {
        $offsets = $this->uri->segment(2);

        // Pagination setting
        $limit      = 10;
        if($offsets > 0)
        {
            $offset = ($offsets - 1) * $limit;
        }
        else
        {
            $offset = 0;
        }

        $news   = $this->news_model->order_by('date', 'desc')->limit($limit, $offset)->find_all_published();

        // Pagination
        $this->load->library('pagination');

        $total_news = $this->news_model->count_all_published();

        $total_page = ceil($total_news / $limit);

        $config['prev_link']                = FALSE;
        $config['next_link']                = FALSE;
        $config['first_link']               = FALSE;
        $config['last_link']                = FALSE;
        $config['cur_tag_open']             = '<li class="pagination__item"><a class="pagination__link is-current">';
        $config['cur_tag_close']            = '</a></li>';
        $config['num_tag_open']             = '<li class="pagination__item">';
        $config['num_tag_close']            = '</li>';
        $config['num_links']                = 2;
        $config['attributes']               = array('class' => 'pagination__link');
        $config['base_url']                 = base_url().lang('news.front_slug');
        $config['total_rows']               = $total_news;
        $config['per_page']                 = $limit;
        $config['use_page_numbers']         = TRUE;
        $config['uri_segment']              = 2;

        $this->pagination->initialize($config);

        $archive_link   = $config['base_url'];
        $nextprev       = nextprevlink_page($offsets,$limit,$total_news,$archive_link);
        $paging         = $this->pagination->create_links();

        $vars = array(
                        'data'              => $news,
                        'nextprev'          => $nextprev,
                        'paging'            => $paging
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('News');
        $this->render('index');
    }

    //--------------------------------------------------------------------


    public function detail_news($slug)
    {
        if($slug == FALSE)
        {
            show_404();
        }

        $detail_news[] = $this->news_model->find_by('slug', $slug);
        if($detail_news)
        {
            $other_news = $this->news_model->order_by('view_count', 'desc')->limit(5)->find_all_by(array('publish'=>1, 'deleted'=>0, 'id !='=>$detail_news[0]->id));

            $data['view_count'] = $detail_news[0]->view_count + 1;
            $add_view_count = $this->news_model->update($detail_news[0]->id, $data);
        }
        else
        {
            show_404();
        }

        $custom_meta_tag = array(
                                'title'         => $detail_news[0]->title,
                                'url'           => base_url().lang('news.slug').'/'.$detail_news[0]->slug,
                                'image'         => config_item('uploads_url').$detail_news[0]->image,
                                'description'   => str_replace(array('<p>', '</p>'), array('', ''),$detail_news[0]->summary)
                            );

        $vars = array(
                        'detail_news'       => $detail_news,
                        'other_news'        => $other_news,
                        'custom_meta_tag'   => $custom_meta_tag,
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title($detail_news[0]->title.' ~ News');
        $this->render('detail');
    }

    //--------------------------------------------------------------------
}

/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */