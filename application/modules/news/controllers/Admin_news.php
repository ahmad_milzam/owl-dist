<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_news extends Admin_Controller
{
	function __construct()
    {
        parent::__construct();

        $this->load->model('news_model');
        $this->lang->load('news');
    }

	//--------------------------------------------------------------------


    public function index()
    {
        // Breadcrumb setting
        $this->load->library('breadcrumb');

        $this->breadcrumb->home_title('<i class="fa fa-home"></i> Home', 'admin');
        $this->breadcrumb->add('News');

        $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);
        // End Breadcrumb setting

        // Pagination setting
        $offsets = $this->input->get('page');
        if($offsets > 0)
        {
            $offset = ($offsets - 1) * $this->data_limit;
        }
        else
        {
            $offset = 0;
        }

        $data_list  = $this->news_model->order_by('created_on', 'desc')->limit($this->data_limit, $offset)->find_all();
        $data_trash = $this->news_model->find_all_deleted();

        $this->load->library('pagination');

        $total_data = $this->news_model->count_all();

        $this->pager_param['base_url']                = site_url(SITE_AREA.'/'.lang('news.slug').'?');
        $this->pager_param['total_rows']              = $total_data;
        $this->pager_param['per_page']                = $this->data_limit;
        $this->pager_param['use_page_numbers']        = TRUE;
        $this->pager_param['enable_query_strings']    = TRUE;
        $this->pager_param['page_query_string']       = TRUE;
        $this->pager_param['query_string_segment']    = "page";

        $this->pagination->initialize($this->pager_param);
        // End Pagination setting

        $vars = array(
                        'page'          => $offsets,
                        'limit'         => $this->data_limit,
                        'data_list'     => $data_list,
                        'data_trash'    => $data_trash,
                        'total_data'    => $total_data,
                        'breadcrumb'    => $breadcrumb
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('News');
        $this->render('news_view');
    }

    //--------------------------------------------------------------------


    public function trash()
    {
        // Breadcrumb setting
        $this->load->library('breadcrumb');

        $this->breadcrumb->home_title('<i class="fa fa-news"></i> Home', 'admin');
        $this->breadcrumb->add('News', SITE_AREA.'/'.lang('news.slug'));
        $this->breadcrumb->add('News Trash');

        $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);
        // End Breadcrumb setting

        // Pagination setting
        $offsets = $this->input->get('page');
        if($offsets > 0)
        {
            $offset = ($offsets - 1) * $this->data_limit;
        }
        else
        {
            $offset = 0;
        }

        $data_list  = $this->news_model->order_by('created_on', 'desc')->limit($this->data_limit, $offset)->find_all_deleted();

        $this->load->library('pagination');

        $total_data = $this->news_model->count_all_deleted();

        $this->pager_param['base_url']                = site_url(SITE_AREA.'/'.lang('news.slug').'?');
        $this->pager_param['total_rows']              = $total_data;
        $this->pager_param['per_page']                = $this->data_limit;
        $this->pager_param['use_page_numbers']        = TRUE;
        $this->pager_param['enable_query_strings']    = TRUE;
        $this->pager_param['page_query_string']       = TRUE;
        $this->pager_param['query_string_segment']    = "page";

        $this->pagination->initialize($this->pager_param);
        // End Pagination setting

        $vars = array(
                        'page'          => $offsets,
                        'limit'         => $this->data_limit,
                        'data_list'     => $data_list,
                        'total_data'    => $total_data,
                        'breadcrumb'    => $breadcrumb
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('News Trash');
        $this->render('news_trash_view');
    }

    //--------------------------------------------------------------------


    public function create()
    {
        if ($this->input->post('submit'))
        {
            if (is_numeric($this->save_data()))
            {
                $this->session->set_flashdata('success_message',lang('news.create_success'));
                redirect(SITE_AREA.'/'.lang('news.slug'));
            }
            else
            {
                $message = lang('news.create_failure');
                $this->render(lang('news.slug').'_create_view');
            }
        }

        // Breadcrumb setting
        $this->load->library('breadcrumb');

        $this->breadcrumb->home_title('<i class="fa fa-news"></i> Home', 'admin');
        $this->breadcrumb->add('News', SITE_AREA.'/'.lang('news.slug'));
        $this->breadcrumb->add('Create News');
        // End Breadcrumb setting

        $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);

        $vars = array(
                        'message'       => isset($message) ? $message : '',
                        'breadcrumb'    => $breadcrumb
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('News - Create News');
        $this->render(lang('news.slug').'_create_view');
    }

    //--------------------------------------------------------------------


    public function edit($id)
    {
        if(empty($id))
        {
            $this->session->set_flashdata('warning_message',lang('news.invalid_id'));
            redirect(SITE_AREA.'/'.lang('news.slug'));
        }

        if ($this->input->post('submit'))
        {
            // var_dump($this->save_data('update', $id));exit();

            if (is_bool($this->save_data('update', $id)))
            {
                $this->session->set_flashdata('success_message',lang('news.edit_success'));
                redirect(SITE_AREA.'/'.lang('news.slug').'/edit/'.$id);
            }
            else
            {
                $message = lang('news.edit_failure');
                $this->render(lang('news.slug').'_edit_view');
            }
        }

        $data[] = $this->news_model->find($id);

        // Breadcrumb setting
        $this->load->library('breadcrumb');

        $this->breadcrumb->home_title('<i class="fa fa-news"></i> Home', 'admin');
        $this->breadcrumb->add('News', SITE_AREA.'/'.lang('news.slug'));
        $this->breadcrumb->add('Edit News');
        // End Breadcrumb setting

        $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);

        $vars = array(
                        'data'          => $data,
                        'message'       => isset($message) ? $message : '',
                        'breadcrumb'    => $breadcrumb
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('News - Edit News');
        $this->render(lang('news.slug').'_edit_view');
    }

    //--------------------------------------------------------------------


    public function move_trash($id)
    {
        if(empty($id))
        {
            $this->session->set_flashdata('warning_message',lang('news.invalid_id'));
            redirect(SITE_AREA.'/'.lang('news.slug'));
        }
        else
        {
            if ($this->news_model->delete($id))
            {
                $this->session->set_flashdata('success_message',lang('news.trash_success'));
            }
            else
            {
                $this->session->set_flashdata('warning_message',$this->news_model->error);
            }

            redirect(SITE_AREA.'/'.lang('news.slug'));
        }
    }

    //--------------------------------------------------------------------


    public function restore($id)
    {
        if(empty($id))
        {
            $this->session->set_flashdata('warning_message',lang('news.invalid_id'));
            redirect(SITE_AREA.'/'.lang('news.slug'));
        }
        else
        {
            if ($this->news_model->restore($id))
            {
                $this->session->set_flashdata('success_message',lang('news.restore_success'));
            }
            else
            {
                $this->session->set_flashdata('warning_message',$this->news_model->error);
            }

            redirect(SITE_AREA.'/'.lang('news.slug'));
        }
    }

    //--------------------------------------------------------------------


    public function delete($id)
    {
        if(empty($id))
        {
            $this->session->set_flashdata('warning_message',lang('news.invalid_id'));
            redirect(SITE_AREA.'/'.lang('news.slug'));
        }
        else
        {
            if ($this->news_model->delete_permanent($id))
            {
                $this->session->set_flashdata('success_message',lang('news.delete_success'));
            }
            else
            {
                $this->session->set_flashdata('warning_message',$this->news_model->error);
            }

            redirect(SITE_AREA.'/'.lang('news.slug'));
        }
    }

    //--------------------------------------------------------------------


    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    private function save_data($type='insert', $id=0)
    {
        // if ($type == 'update') {
        //     $_POST['id'] = $id;
        // }

        $this->load->library('form_validation');

        if($type == "insert")
        {
            $this->form_validation->set_rules('title','Title','required|trim|is_unique[news.title]|max_length[255]');
        }
        else
        {
            $this->form_validation->set_rules('title','Title','required|trim|max_length[255]');
        }
        $this->form_validation->set_rules('summary','Summary','trim|max_length[255]');
        $this->form_validation->set_rules('content','Content','required|trim');
        $this->form_validation->set_rules('date','Date','required|trim');
        $this->form_validation->set_rules('image','Image','trim|max_length[255]');
        $this->form_validation->set_rules('publish','Publish','required|trim|integer|max_length[1]');

        $this->form_validation->set_error_delimiters('<p>', '</p>');

        if($this->form_validation->run() === FALSE)
        {
            return validation_errors();
        }
        else
        {
            $input_date     = explode('-', $this->input->post('date'));
            $date           = $input_date[2].'-'.$input_date[1].'-'.$input_date[0];

            $data = array(
                        'title'         => $this->input->post('title'),
                        'summary'       => $this->input->post('summary'),
                        'content'       => $this->input->post('content'),
                        'date'          => $date,
                        'slug'          => generate_slug($this->input->post('title')),
                        'publish'       => $this->input->post('publish'),
                    );

            if($this->input->post('image') != "")
            {
                $data['image'] = 'files/'.$this->input->post('image');
            }

            if ($type == 'insert')
            {
                $id = $this->news_model->insert($data);

                if (is_numeric($id))
                {
                    $return = $id;
                }
                else
                {
                    $return = FALSE;
                }
            }
            elseif ($type == 'update')
            {
                $return = $this->news_model->update($id, $data);
            }

            return $return;
            // print_r($return);exit();
        }
    }

    //--------------------------------------------------------------------
}