<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// General
$lang['news.name']			            	= 'News';
$lang['news.slug']			            	= 'news';
$lang['news.front_slug']			        = 'news';

// Page Components
$lang['news.header_title']			    	= 'News';
$lang['news.content_title']			    	= 'News List';
$lang['news.content_trash_title']			= 'News Trash';

$lang['news.create_success']			    = 'News successfully created.';
$lang['news.create_failure']			    = 'There was a problem creating news.';
$lang['news.create_button']					= 'Create News';

$lang['news.edit_caption']			    	= 'Edit this news';
$lang['news.edit_success']			    	= 'News successfully edited.';
$lang['news.edit_failure']			    	= 'There was a problem editing news.';
$lang['news.edit_submit_button']			= 'Edit News';
$lang['news.edit_button']					= 'Edit';

$lang['news.trash_caption']			    	= 'Move this news to the Trash';
$lang['news.trash_success']			    	= 'News successfully moved to the trash.';
$lang['news.trash_failure']			    	= 'There was a problem moving news to the trash.';
$lang['news.trash_confirm']					= 'Are you sure you want to move this news to the trash?';
$lang['news.trash_button']					= 'Trash';

$lang['news.restore_caption']			    = 'Restore this news from the Trash';
$lang['news.restore_success']			    = 'News successfully restored.';
$lang['news.restore_failure']			    = 'There was a problem restoring news.';
$lang['news.restore_confirm']				= 'Are you sure you want to restore this news?';
$lang['news.restore_button']				= 'Restore';

$lang['news.delete_caption']			    = 'Delete this news permanently';
$lang['news.delete_success']			    = 'News successfully deleted.';
$lang['news.delete_failure']			    = 'There was a problem deleting news.';
$lang['news.delete_confirm']				= 'Are you sure you want to delete this news permanently?';
$lang['news.delete_button']					= 'Delete';

$lang['news.invalid_id']			        = 'Invalid data ID.';

$lang['news.content_create_title']			= 'Create News';
$lang['news.content_edit_title']			= 'Edit News';

$lang['news.back_button']					= 'Back';
$lang['news.cancel_button']					= 'Cancel';
