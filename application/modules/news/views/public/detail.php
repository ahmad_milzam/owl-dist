<!-- page content -->
<div class="section section--space-top">

  <div class="grid">
    <div class="grid__item small-1 medium-2of3">
      <article class="article">
        <header class="article__header">
          <h1 class="article__title">
            <?php echo $detail_news[0]->title; ?>
          </h1>
          <div class="article__date"><?php echo date('F, j Y',strtotime($detail_news[0]->date));?></div>
        </header>

        <img class="article__img" src="<?php echo config_item('uploads_url').$detail_news[0]->image; ?>" alt="image">

        <?php echo $detail_news[0]->content; ?>
      </article>
    </div>

    <?php if(! empty($other_news)) : ?>
    <div class="grid__item small-1 medium-1of3">
      <div class="submenu submenu--primary">
        <div class="submenu__head">
          Popular News
        </div>
        <ul class="submenu__list">
          <?php foreach($other_news AS $news) : ?>
          <li class="submenu__item">
            <a href="<?php echo base_url().lang('news.front_slug').'/'.$news->slug ?>" class="submenu__link">
              <?php echo $news->title; ?>
              <small class="text-disabled"><?php echo date('F, j Y',strtotime($news->date));?></small>
            </a>
          </li>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>
    <?php endif; ?>
  </div>

</div>
<!-- page content -->