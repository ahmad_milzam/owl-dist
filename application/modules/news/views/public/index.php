<!-- page content -->

<div class="section">
  <div class="grid">
    <div class="grid__item small-1">
      <h1 class="heading">News</h1>
    </div>
  </div>

  <?php if(! empty($data)) : ?>
  <div class="grid">
    <?php foreach($data AS $news) : ?>
    <div class="grid__item small-1 medium-1of2">
      <article class="article">
        <img class="article__img" src="<?php echo config_item('uploads_url').str_replace('files', 'thumbs', $news->image); ?>" alt="<?php echo $news->title; ?>">

        <header class="article__header">
          <h3 class="article__title article__title--medium">
            <?php echo $news->title; ?>
          </h3>
          <div class="article__date"><?php echo date('F, j Y',strtotime($news->date));?></div>
        </header>

        <p>
          <?php echo $news->summary; ?>
        </p>
        <a href="<?php echo base_url().lang('news.front_slug').'/'.$news->slug; ?>" class="btn btn--ghost">
          <span class="btn__valign">Read More</span>
          <svg class="icon icon--chevron-right btn__valign"><use xlink:href="#icon--chevron-right"></use></svg>
        </a>
      </article>
    </div>
    <?php endforeach; ?>
  </div>
  <?php endif; ?>
</div>
<!-- page content -->

<?php
  if(isset($nextprev['prev_link']))
  {
    $prev_tag_open='<li class="pagination__item"><a href="'.$nextprev['prev_link'].'" class="pagination__link is-button">Prev';
    $prev_tag_close='</a></li>';
  }
  elseif(!isset($nextprev['prev_link']))
  {
    $prev_tag_open='<li class="pagination__item"><a class="pagination__link is-button is-disabled">Prev';
    $prev_tag_close='</a></li>';
  }

  if(isset($nextprev['next_link']))
  {
    $next_tag_open='<li class="pagination__item"><a href="'.$nextprev['next_link'].'" class="pagination__link is-button">Next';
    $next_tag_close='</a></li>';
  }
  elseif(!isset($nextprev['next_link']))
  {
    $next_tag_open='<li class="pagination__item"><a class="pagination__link is-button is-disabled">Next';
    $next_tag_close='</a></li>';
  }
?>

<?php if(!empty($nextprev)) : ?>
<!-- pagination -->
<div class="section">
  <div class="grid">
    <div class="grid__item small-1">
      <ul class="pagination pagination--seperate pagination--responsive">
        <?php echo $prev_tag_open; ?><?php echo $prev_tag_close; ?>
        <?php echo $paging; ?>
        <?php echo $next_tag_open; ?><?php echo $next_tag_close; ?>
      </ul>
    </div>
  </div>
</div>
<!-- pagination -->
<?php endif; ?>