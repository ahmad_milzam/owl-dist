<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
		<?php echo lang('news.header_title'); ?>
		</h1>
		<?php echo $breadcrumb; ?>
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title"><?php echo lang('news.content_edit_title'); ?></h3>
					</div><!-- /.box-header -->

					<?php echo form_open(); ?>

					<?php
						$error_class 	= 'has-error';
						$error_icon 	= '<i class="fa fa-times-circle-o"></i>';
					?>

						<div class="box-body">
							<?php if(isset($message) && ! empty($message)) : ?>
							<div class="alert alert-danger alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<h4><i class="icon fa fa-ban"></i> Error!</h4>
								<?php echo $message; ?>
							</div>
						    <?php endif; ?>

						    <?php if($this->session->flashdata('success_message') != "") : ?>
							<div class="alert alert-success alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<h4><i class="icon fa fa-check"></i> Success!</h4>
								<?php echo $this->session->flashdata('success_message'); ?>
							</div>
						    <?php endif; ?>

							<!-- Title -->
							<div class="form-group <?php echo form_error('title') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('title') ? $error_icon : ''; ?> Title*
								</label>
								<input type="text" name="title" class="form-control" value="<?php echo set_value('title', isset($data[0]->title) ? $data[0]->title : ''); ?>" required>
								<p class="help-block"><?php echo form_error('title');?></p>
							</div>

							<!-- Summary -->
							<div class="form-group <?php echo form_error('summary') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('summary') ? $error_icon : ''; ?> Summary
								</label>
								<textarea id="summary" name="summary" cols="80" rows="5" class="wysiwyg-simple-br"><?php echo isset($data[0]->summary) ? $data[0]->summary : ''; ?></textarea>
								<p class="help-block"><?php echo form_error('summary');?></p>
							</div>

							<!-- Content -->
							<div class="form-group <?php echo form_error('content') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('content') ? $error_icon : ''; ?> Content*
								</label>
                    			<textarea id="content" name="content" cols="80" rows="5" class="wysiwyg-advanced"><?php echo isset($data[0]->content) ? $data[0]->content : ''; ?></textarea>
								<p class="help-block"><?php echo form_error('content');?></p>
							</div>

							<!-- Date -->
							<div class="form-group <?php echo form_error('date') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('date') ? $error_icon : ''; ?> Date*
								</label>
								<div class="input-group">
								    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
								    <?php
								    	if(! empty($data[0]->date))
								    	{
								    		$date_value = explode("-", $data[0]->date);
								    		$date_value = $date_value[2].'-'.$date_value[1].'-'.$date_value[0];
								    	}
								    	else
								    	{
								    		$date_value = "";
								    	}
								    ?>
									<input type="text" class="input-datepicker form-control" name="date" value="<?php echo $date_value; ?>" required/>
								</div>
								<p class="help-block"><?php echo form_error('date');?></p>
							</div>

							<!-- Image -->
							<div class="form-group <?php echo form_error('image') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('image') ? $error_icon : ''; ?> Image
								</label>
								<?php if(! empty($data[0]->image)) : ?>
								<img src="<?php echo config_item('uploads_url').$data[0]->image; ?>" style="display: block; margin: 5px 0 10px 0; width: 100px;">
								<?php endif; ?>
								<div class="input-group">
									<div class="input-group-btn">
										<a href="<?php echo base_url().'filemanager/dialog.php?type=1&field_id=image_path&relative_url=1'; ?>" class="file-iframe-btn" data-fancybox-type="iframe">
											<button type="button" class="btn btn-block btn-default btn-flat">Browse</button>
										</a>
									</div>
									<input id="image_path" name="image" type="text" class="form-control">
								</div>
							</div>

							<!-- Publish -->
							<div class="form-group <?php echo form_error('publish') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('publish') ? $error_icon : ''; ?> Publish*
								</label>
								<select name="publish" class="form-control">
									<option value="0" <?php echo isset($data[0]->publish) && $data[0]->publish == "0" ? "selected" : ""; ?>>No</option>
									<option value="1" <?php echo isset($data[0]->publish) && $data[0]->publish == "1" ? "selected" : ""; ?>>Yes</option>
								</select>
								<p class="help-block"><?php echo form_error('publish');?></p>
							</div>

						</div><!-- /.box-body -->

						<div class="box-footer">
							<input type="submit" name="submit" class="btn btn-primary" value="<?php echo lang('news.edit_button') ?>">
							&nbsp;
							<a href="<?php echo base_url().'admin/'.lang('news.slug'); ?>" class="btn btn-default">
								<?php echo lang('news.cancel_button') ?>
							</a>
						</div>
					<?php echo form_close();?>
				</div><!-- /.box -->
			</div><!--/.col (right) -->
		</div>

	</section><!-- /.content -->

</div><!-- /.content-wrapper -->