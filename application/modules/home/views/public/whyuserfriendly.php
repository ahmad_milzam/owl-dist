<!-- page content -->

<div class="section">
  <div class="grid">
    <div class="grid__item small-1">
      <h1 class="heading">Why OWL</h1>
    </div>
  </div>
  <div class="grid">
    <div class="grid__item small-1 medium-1of3">
      <!-- submenu -->
      <div class="submenu">
        <ul class="submenu__list">
          <li class="submenu__item">
            <a href="<?php echo base_url('why/user-friendly') ?>" class="submenu__link is-active">
              <svg class="icon icon--thick submenu__align"><use xlink:href="#icon--thick"></use></svg>
              <span class="submenu__align">User Friendly &amp; Easy to Use</span>
              <svg class="icon icon--chevron-right submenu__arrow"><use xlink:href="#icon--chevron-right"></use></svg>
            </a>
          </li>
          <li class="submenu__item">
            <a href="<?php echo base_url('why/security') ?>" class="submenu__link">
              <svg class="icon icon--lock submenu__align"><use xlink:href="#icon--lock"></use></svg>
              <span class="submenu__align">Security &amp; Authorization</span>
              <svg class="icon icon--chevron-right submenu__arrow"><use xlink:href="#icon--chevron-right"></use></svg>
            </a>
          </li>
          <li class="submenu__item">
            <a href="<?php echo base_url('why/bussiness-overview') ?>" class="submenu__link">
              <svg class="icon icon--chart submenu__align"><use xlink:href="#icon--chart"></use></svg>
              <span class="submenu__align">Business Process Overview</span>
              <svg class="icon icon--chevron-right submenu__arrow"><use xlink:href="#icon--chevron-right"></use></svg>
            </a>
          </li>
          <li class="submenu__item">
            <a href="<?php echo base_url('why/detail-of-module') ?>" class="submenu__link">
              <svg class="icon icon--module submenu__align"><use xlink:href="#icon--module"></use></svg>
              <span class="submenu__align">Detail of Module</span>
              <svg class="icon icon--chevron-right submenu__arrow"><use xlink:href="#icon--chevron-right"></use></svg>
            </a>
          </li>
        </ul>
      </div>
      <!-- submenu -->
    </div>
    <div class="grid__item small-1 medium-2of3">
      <img src="http://placehold.it/1000x500" alt="image">
      <br>
      <h3>Subheading title</h3>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </p>
    </div>
  </div>
</div>
<!-- page content -->