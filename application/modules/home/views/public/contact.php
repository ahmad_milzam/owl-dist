<!-- page content -->
<div class="section">
  <div class="grid">
    <div class="grid__item small-1">
      <h1 class="heading">Contact OWL</h1>
    </div>
  </div>
  <div class="grid">
    <div class="grid__item small-1 medium-1of2">
      <h3 class="subheading">Yes, We Are Ready to Help You!</h3>
      <div id="js-alertContainer"></div>
      <form class="mb" action="<?php echo base_url('submit_contact_form'); ?>" method="post" id="js-form">
        <div class="field-row">
          <label class="label label--required" for="name">Name</label>
          <input name="name" type="text" class="input input--full" id="name" required>
        </div>
        <div class="field-row">
          <label class="label label--required" for="email">Email</label>
          <input name="email" type="email" class="input input--full" id="email" required>
        </div>
        <div class="field-row">
          <label class="label label--required" for="phone">Phone Number</label>
          <input name="phone" type="tel" class="input input--full" id="phone" required pattern="[0-9]+">
        </div>
        <div class="field-row">
          <label class="label label--required" for="message">Message</label>
          <textarea name="message" class="input input--full input--textarea" id="message" rows="5" required></textarea>
        </div>
        <div class="field-row">
          <input type="text" name="first_name" style="display: none;">
          <button class="btn btn--ghost btn--full" type="submit">SEND</button>
        </div>
      </form>
    </div>
    <div class="grid__item small-1 medium-1of2">
      <div class="map" id="map"></div>
      <script>
        var map,
            marker;

        function initMap() {
          var myLatLng = {lat: -6.24617, lng: 106.803443};

          map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            zoom: 16
          });

          marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Our Office'
          });
        }
      </script>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC94SZzkODyO2UbU3ksaIjFjfT-3V5CY3U&callback=initMap"
      async defer></script>
    </div>
  </div>
</div>
<!-- page content -->