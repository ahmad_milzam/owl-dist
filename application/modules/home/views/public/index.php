<!-- slide container -->
<div class="slide">
  <!-- actual slide -->
  <div class="slide__component" id="js-slide">
	<?php if(! empty($banners_list)) : ?>
		<?php foreach($banners_list AS $banner) : ?>
			<div class="slide__item">

      <?php if (!empty($banner->title) || !empty($banner->sub_title)): ?>
        <div class="slide__content">
          <?php if (!empty($banner->title)): ?>
          <div class="slide__headline"><?php echo $banner->title; ?></div>
          <?php endif ?>

          <?php if (!empty($banner->sub_title)): ?>
          <div class="slide__tagline"><?php echo $banner->sub_title; ?></div>
          <?php endif ?>

          <?php if (!empty($banner->url)): ?>
            <a href="<?php echo $banner->url; ?>" class="btn btn--ghost btn--white slide__btn">
              <span class="btn__valign">Learn More</span>
              <svg class="icon icon--chevron-right btn__valign"><use xlink:href="#icon--chevron-right"></use></svg>
            </a>
          <?php endif ?>
        </div>
      <?php endif ?>

      <?php
        $image_small_url  = explode('/', $banner->image_small);
        $image_small      = end($image_small_url);

        $image_large_url  = explode('/', $banner->image_large);
        $image_large      = end($image_large_url);
      ?>
	      <img class="img-responsive"
	        src="<?php echo config_item('assets_url'); ?>front/img/blank.png"
	        alt="<?php echo $banner->image_alt_text; ?>"
	        data-src-base="<?php echo config_item('uploads_url').'files/banners/' ?>"
	        data-src="<760:small/<?php echo $image_small; ?>,
	                  >760:large/<?php echo $image_large; ?>"/>
	    </div>
		<?php endforeach; ?>
	<?php endif; ?>
  </div><!-- actual slide -->

  <!-- slide navigation -->
  <!-- <a href="#" class="slide__nav slide__nav--prev">Prev</a> -->
  <!-- <a href="#" class="slide__nav slide__nav--next">Next</a> -->
  <!-- slide navigation -->
</div><!-- slide container -->

<!-- section welcome -->
<div class="section section--dark">
  <div class="grid">
    <div class="grid__item small-1 medium-2of3 medium-centered text-center">
      <h2 class="heading">Welcome</h2>
      <p class="lede">
        OWL-Plantation System is an Enterprise Resource Planning (ERP) for Palm Oil Plantations and Mill.
        We have developed OWL-Plantation System since 2009 and is just perfect OWL-Plantation System simplifies you administration system from the lowest level to the highest level,
        maintaining data consistency, reduce loses, facilitate control and help in decision-making.
      </p>
    </div>
  </div>
</div>
<!-- section welcome -->

<!-- section why -->
<div class="section section--light">
  <div class="grid">
    <div class="grid__item small-1">
      <h2 class="heading">Why OWL</h2>
    </div>
  </div>
  <div class="grid">
    <div class="grid__item small-1of2 medium-1of4">
      <a href="<?php echo base_url('why/user-friendly') ?>" class="promote">
        <div class="promote__circle">
          <svg class="icon icon--thick promote__icon"><use xlink:href="#icon--thick"></use></svg>
        </div>
        <div class="promote__text">User Friendly &amp; Easy to Use</div>
      </a>
    </div>
    <div class="grid__item small-1of2 medium-1of4">
      <a href="<?php echo base_url('why/security') ?>" class="promote">
        <div class="promote__circle">
          <svg class="icon icon--lock promote__icon"><use xlink:href="#icon--lock"></use></svg>
        </div>
        <div class="promote__text">Security &amp; Authorization</div>
      </a>
    </div>
    <div class="grid__item small-1of2 medium-1of4">
      <a href="<?php echo base_url('why/bussiness-overview') ?>" class="promote">
        <div class="promote__circle">
          <svg class="icon icon--chart promote__icon"><use xlink:href="#icon--chart"></use></svg>
        </div>
        <div class="promote__text">Business Process Overview</div>
      </a>
    </div>
    <div class="grid__item small-1of2 medium-1of4">
      <a href="<?php echo base_url('why/detail-of-module') ?>" class="promote">
        <div class="promote__circle">
          <svg class="icon icon--module promote__icon"><use xlink:href="#icon--module"></use></svg>
        </div>
        <div class="promote__text">Detail of Module</div>
      </a>
    </div>
  </div>
</div>
<!-- section why -->

<!-- latest news and update -->
<div class="duo-color">
  <div class="grid">

    <div class="grid__item small-1 medium-1of2 duo-color__first">
      <h2>News</h2>
      <?php if(! empty($news_list)) : ?>

      <?php foreach($news_list AS $news) : ?>
      <div class="article">
        <h3 class="article__title article__title--small"><?php echo $news->title; ?></h3>
        <div class="article__date article__date--white"><?php echo date('F, j Y',strtotime($news->date));?></div>
        <p>
          <?php echo $news->summary; ?>
        </p>
        <a href="<?php echo base_url().lang('news.front_slug').'/'.$news->slug; ?>" class="btn btn--ghost btn--white">
          <span class="btn__valign">Read More</span>
          <svg class="icon icon--chevron-right btn__valign"><use xlink:href="#icon--chevron-right"></use></svg>
        </a>
      </div>
    	<?php endforeach; ?>

      <?php endif; ?>
    </div>

    <div class="grid__item small-1 medium-1of2 duo-color__last">
      <h2>Update</h2>
      <?php if(! empty($files_list)) : ?>

      <?php foreach($files_list AS $files) : ?>
      <div class="update">
        <h3 class="update__title"><?php echo $files->title; ?></h3>
        <div class="grid">
          <div class="grid__item small-1of2"><?php echo date('F, j Y',strtotime($files->date));?></div>
          <div class="grid__item small-1of2 text-right">
            <a href="<?php echo config_item('uploads_url').$files->file; ?>" class="btn btn--ghost btn--white btn--small" target="_blank">
              <span class="btn__valign">Download Here</span>
            </a>
          </div>
        </div>
      </div>
    	<?php endforeach; ?>

      <?php endif; ?>
    </div>

  </div>
</div>
<!-- latest news and update -->