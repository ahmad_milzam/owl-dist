<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Public_Controller
{
    function __construct()
    {
        parent::__construct();
    }

	//--------------------------------------------------------------------



    /*
        Method: index()

        Display homepage.
    */
    public function index()
    {
        $this->load->model('banners/banners_model');
        $this->load->model('news/news_model');
        $this->load->model('files/files_model');

        $this->lang->load('news/news');

        $banners_list   = $this->banners_model->find_all();
        $news_list      = $this->news_model->order_by('date', 'desc')->limit(3)->find_all_published();
        $files_list     = $this->files_model->order_by('date', 'desc')->limit(5)->find_all_published();


        $vars = array(
                        'banners_list'  => $banners_list,
                        'news_list'     => $news_list,
                        'files_list'    => $files_list,
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->render('index');
    }

    //--------------------------------------------------------------------



    /*
        Method: about()

        Display about page.
    */
    public function about()
    {
        $this->template->title('About OWL');
        $this->render('about');
    }

    //--------------------------------------------------------------------



    /*
        Method: why()

        Display why page.
    */
    public function why()
    {
        redirect(base_url('why/user-friendly'),'refresh');
    }

    public function whyuserfriendly()
    {
        $this->template->title('User Friendly & Easy to Use');
        $this->render('whyuserfriendly');
    }

    public function whysecurity()
    {
        $this->template->title('Security & Authorization');
        $this->render('whysecurity');
    }

    public function whybussiness()
    {
        $this->template->title('Business Process Overview');
        $this->render('whybussiness');
    }

    public function whymodule()
    {
        $this->template->title('Detail of Module');
        $this->render('whymodule');
    }

    //--------------------------------------------------------------------



    /*
        Method: contact()

        Display contact page.
    */
    public function contact()
    {
        $this->template->title('Contact');
        $this->render('contact');
    }

    //--------------------------------------------------------------------



    /*
        Method: submit_contact_form()

        Submit contact form.
    */
    public function submit_contact_form()
    {
        $this->load->model('contacts_model');
        $this->load->library('form_validation');

        $settings = $this->settings_lib->find_all();

        $error;

        $this->form_validation->set_rules('name','Name','required|trim|max_length[255]');
        $this->form_validation->set_rules('email','Email','required|trim|valid_email|max_length[255]');
        $this->form_validation->set_rules('phone','Phone','required|trim|numeric|max_length[15]');
        $this->form_validation->set_rules('message','Message','required|trim');

        $this->form_validation->set_error_delimiters('<li>', '</li>');

        if ($this->form_validation->run() === FALSE)
        {
            $error = $this->form_validation->error_string();
        }
        else
        {
            $hidden_field = $this->input->post('first_name');
            if (empty($hidden_field))
            {
                $data['name']           = $this->input->post('name');
                $data['email']          = $this->input->post('email');
                $data['phone']          = $this->input->post('phone');
                $data['message']        = $this->input->post('message');

                $submit = $this->contacts_model->insert($data);
                if(! $submit)
                {
                    $error = "<li>Submit form failed, please try again.</li>";
                }
                else
                {
                    if(defined('ENVIRONMENT') && ENVIRONMENT != "development")
                    {
                        $config['charset'] = 'iso-8859-1';
                        $config['wordwrap'] = FALSE;
                        $config['validate'] = TRUE;
                        $config['mailtype'] = 'html';

                        $this->email->initialize($config);

                        //email setting to support email
                        $this->email->from('admin@owl-plantation.org', 'OWL Plantation Web');
                        $this->email->to($settings['site_email']);
                        $this->email->reply_to($data['email'], $data['name']);

                        $this->email->subject('OWL Plantation Contact Form');
                        $this->email->message("Name : ".$data['name']."<br> Email : ".$data['email']."<br> Phone : ".$data['phone']."<br> Messages : ".$data['message']);

                        $this->email->send();
                    }
                }
            }
        }

        if(!empty($error)) //kondisi kalo ada error alias form submit gagal
        {
            $return['success']   = 0;
            $return['error']     = $error;
        }
        else //kalo gak ada error alias form submit sukses
        {
            $return['success']   = 1;
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($return));
    }

    //--------------------------------------------------------------------
}

/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */