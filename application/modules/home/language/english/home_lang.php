<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// General
$lang['module.name']			            = 'Home';
$lang['module.slug']			            = 'home';

// Page Components
$lang['module.header_title']			    = 'Homepage';
$lang['module.content_title']			    = 'Homepage List';
$lang['module.content_trash_title']			= 'Homepage Trash';

$lang['module.create_success']			    = 'Homepage successfully created.';
$lang['module.create_failure']			    = 'There was a problem creating homepage.';
$lang['module.create_button']				= 'Create Homepage';

$lang['module.edit_caption']			    = 'Edit this homepage';
$lang['module.edit_success']			    = 'Homepage successfully edited.';
$lang['module.edit_failure']			    = 'There was a problem editing homepage.';
$lang['module.edit_submit_button']			= 'Edit Homepage';
$lang['module.edit_button']					= 'Edit';

$lang['module.trash_caption']			    = 'Move this homepage to the Trash';
$lang['module.trash_success']			    = 'Homepage successfully moved to the trash.';
$lang['module.trash_failure']			    = 'There was a problem moving homepage to the trash.';
$lang['module.trash_confirm']				= 'Are you sure you want to move this homepage to the trash?';
$lang['module.trash_button']				= 'Trash';

$lang['module.restore_caption']			    = 'Restore this homepage from the Trash';
$lang['module.restore_success']			    = 'Homepage successfully restored.';
$lang['module.restore_failure']			    = 'There was a problem restoring homepage.';
$lang['module.restore_confirm']				= 'Are you sure you want to restore this homepage?';
$lang['module.restore_button']				= 'Restore';

$lang['module.delete_caption']			    = 'Delete this homepage permanently';
$lang['module.delete_success']			    = 'Homepage successfully deleted.';
$lang['module.delete_failure']			    = 'There was a problem deleting homepage.';
$lang['module.delete_confirm']				= 'Are you sure you want to delete this homepage permanently?';
$lang['module.delete_button']				= 'Delete';

$lang['module.invalid_id']			        = 'Invalid data ID.';

$lang['module.content_create_title']		= 'Create New Homepage';
$lang['module.content_edit_title']			= 'Edit Homepage';

$lang['module.back_button']					= 'Back';
$lang['module.cancel_button']				= 'Cancel';
