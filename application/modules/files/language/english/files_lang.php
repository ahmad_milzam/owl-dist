<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// General
$lang['files.name']			            	= 'Files';
$lang['files.slug']			            	= 'files';
$lang['files.front_slug']			        = 'files';

// Page Components
$lang['files.header_title']			    	= 'Files';
$lang['files.content_title']			    = 'Files List';
$lang['files.content_trash_title']			= 'Files Trash';

$lang['files.create_success']			    = 'File successfully created.';
$lang['files.create_failure']			    = 'There was a problem creating file.';
$lang['files.create_button']				= 'Create File';

$lang['files.edit_caption']			    	= 'Edit this file';
$lang['files.edit_success']			    	= 'File successfully edited.';
$lang['files.edit_failure']			    	= 'There was a problem editing file.';
$lang['files.edit_submit_button']			= 'Edit File';
$lang['files.edit_button']					= 'Edit';

$lang['files.trash_caption']			    = 'Move this file to the Trash';
$lang['files.trash_success']			    = 'File successfully moved to the trash.';
$lang['files.trash_failure']			    = 'There was a problem moving file to the trash.';
$lang['files.trash_confirm']				= 'Are you sure you want to move this file to the trash?';
$lang['files.trash_button']					= 'Trash';

$lang['files.restore_caption']			    = 'Restore this file from the Trash';
$lang['files.restore_success']			    = 'File successfully restored.';
$lang['files.restore_failure']			    = 'There was a problem restoring file.';
$lang['files.restore_confirm']				= 'Are you sure you want to restore this file?';
$lang['files.restore_button']				= 'Restore';

$lang['files.delete_caption']			    = 'Delete this file permanently';
$lang['files.delete_success']			    = 'File successfully deleted.';
$lang['files.delete_failure']			    = 'There was a problem deleting file.';
$lang['files.delete_confirm']				= 'Are you sure you want to delete this file permanently?';
$lang['files.delete_button']				= 'Delete';

$lang['files.invalid_id']			        = 'Invalid data ID.';

$lang['files.content_create_title']			= 'Create File';
$lang['files.content_edit_title']			= 'Edit File';

$lang['files.back_button']					= 'Back';
$lang['files.cancel_button']				= 'Cancel';
