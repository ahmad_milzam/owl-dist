<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_files extends Admin_Controller
{
	function __construct()
    {
        parent::__construct();

        $this->load->model('files_model');
        $this->lang->load('files');
    }

	//--------------------------------------------------------------------


    public function index()
    {
        // Breadcrumb setting
        $this->load->library('breadcrumb');

        $this->breadcrumb->home_title('<i class="fa fa-home"></i> Home', 'admin');
        $this->breadcrumb->add('Files');

        $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);
        // End Breadcrumb setting

        // Pagination setting
        $offsets = $this->input->get('page');
        if($offsets > 0)
        {
            $offset = ($offsets - 1) * $this->data_limit;
        }
        else
        {
            $offset = 0;
        }

        $data_list  = $this->files_model->order_by('created_on', 'desc')->limit($this->data_limit, $offset)->find_all();
        $data_trash = $this->files_model->find_all_deleted();

        $this->load->library('pagination');

        $total_data = $this->files_model->count_all();

        $this->pager_param['base_url']                = site_url(SITE_AREA.'/'.lang('files.slug').'?');
        $this->pager_param['total_rows']              = $total_data;
        $this->pager_param['per_page']                = $this->data_limit;
        $this->pager_param['use_page_numbers']        = TRUE;
        $this->pager_param['enable_query_strings']    = TRUE;
        $this->pager_param['page_query_string']       = TRUE;
        $this->pager_param['query_string_segment']    = "page";

        $this->pagination->initialize($this->pager_param);
        // End Pagination setting

        $vars = array(
                        'page'          => $offsets,
                        'limit'         => $this->data_limit,
                        'data_list'     => $data_list,
                        'data_trash'    => $data_trash,
                        'total_data'    => $total_data,
                        'breadcrumb'    => $breadcrumb
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('Files');
        $this->render('files_view');
    }

    //--------------------------------------------------------------------


    public function trash()
    {
        // Breadcrumb setting
        $this->load->library('breadcrumb');

        $this->breadcrumb->home_title('<i class="fa fa-files"></i> Home', 'admin');
        $this->breadcrumb->add('Files', SITE_AREA.'/'.lang('files.slug'));
        $this->breadcrumb->add('Files Trash');

        $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);
        // End Breadcrumb setting

        // Pagination setting
        $offsets = $this->input->get('page');
        if($offsets > 0)
        {
            $offset = ($offsets - 1) * $this->data_limit;
        }
        else
        {
            $offset = 0;
        }

        $data_list  = $this->files_model->order_by('created_on', 'desc')->limit($this->data_limit, $offset)->find_all_deleted();

        $this->load->library('pagination');

        $total_data = $this->files_model->count_all_deleted();

        $this->pager_param['base_url']                = site_url(SITE_AREA.'/'.lang('files.slug').'?');
        $this->pager_param['total_rows']              = $total_data;
        $this->pager_param['per_page']                = $this->data_limit;
        $this->pager_param['use_page_numbers']        = TRUE;
        $this->pager_param['enable_query_strings']    = TRUE;
        $this->pager_param['page_query_string']       = TRUE;
        $this->pager_param['query_string_segment']    = "page";

        $this->pagination->initialize($this->pager_param);
        // End Pagination setting

        $vars = array(
                        'page'          => $offsets,
                        'limit'         => $this->data_limit,
                        'data_list'     => $data_list,
                        'total_data'    => $total_data,
                        'breadcrumb'    => $breadcrumb
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('Files Trash');
        $this->render('files_trash_view');
    }

    //--------------------------------------------------------------------


    public function create()
    {
        if ($this->input->post('submit'))
        {
            if (is_numeric($this->save_data()))
            {
                $this->session->set_flashdata('success_message',lang('files.create_success'));
                redirect(SITE_AREA.'/'.lang('files.slug'));
            }
            else
            {
                $message = lang('files.create_failure');
                $this->render(lang('files.slug').'_create_view');
            }
        }

        // Breadcrumb setting
        $this->load->library('breadcrumb');

        $this->breadcrumb->home_title('<i class="fa fa-files"></i> Home', 'admin');
        $this->breadcrumb->add('Files', SITE_AREA.'/'.lang('files.slug'));
        $this->breadcrumb->add('Create Files');
        // End Breadcrumb setting

        $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);

        $vars = array(
                        'message'       => isset($message) ? $message : '',
                        'breadcrumb'    => $breadcrumb
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('Files - Create Files');
        $this->render(lang('files.slug').'_create_view');
    }

    //--------------------------------------------------------------------


    public function edit($id)
    {
        if(empty($id))
        {
            $this->session->set_flashdata('warning_message',lang('files.invalid_id'));
            redirect(SITE_AREA.'/'.lang('files.slug'));
        }

        if ($this->input->post('submit'))
        {
            // var_dump($this->save_data('update', $id));exit();

            if (is_bool($this->save_data('update', $id)))
            {
                $this->session->set_flashdata('success_message',lang('files.edit_success'));
                redirect(SITE_AREA.'/'.lang('files.slug').'/edit/'.$id);
            }
            else
            {
                $message = lang('files.edit_failure');
                $this->render(lang('files.slug').'_edit_view');
            }
        }

        $data[] = $this->files_model->find($id);

        // Breadcrumb setting
        $this->load->library('breadcrumb');

        $this->breadcrumb->home_title('<i class="fa fa-files"></i> Home', 'admin');
        $this->breadcrumb->add('Files', SITE_AREA.'/'.lang('files.slug'));
        $this->breadcrumb->add('Edit Files');
        // End Breadcrumb setting

        $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);

        $vars = array(
                        'data'          => $data,
                        'message'       => isset($message) ? $message : '',
                        'breadcrumb'    => $breadcrumb
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('Files - Edit Files');
        $this->render(lang('files.slug').'_edit_view');
    }

    //--------------------------------------------------------------------


    public function move_trash($id)
    {
        if(empty($id))
        {
            $this->session->set_flashdata('warning_message',lang('files.invalid_id'));
            redirect(SITE_AREA.'/'.lang('files.slug'));
        }
        else
        {
            if ($this->files_model->delete($id))
            {
                $this->session->set_flashdata('success_message',lang('files.trash_success'));
            }
            else
            {
                $this->session->set_flashdata('warning_message',$this->files_model->error);
            }

            redirect(SITE_AREA.'/'.lang('files.slug'));
        }
    }

    //--------------------------------------------------------------------


    public function restore($id)
    {
        if(empty($id))
        {
            $this->session->set_flashdata('warning_message',lang('files.invalid_id'));
            redirect(SITE_AREA.'/'.lang('files.slug'));
        }
        else
        {
            if ($this->files_model->restore($id))
            {
                $this->session->set_flashdata('success_message',lang('files.restore_success'));
            }
            else
            {
                $this->session->set_flashdata('warning_message',$this->files_model->error);
            }

            redirect(SITE_AREA.'/'.lang('files.slug'));
        }
    }

    //--------------------------------------------------------------------


    public function delete($id)
    {
        if(empty($id))
        {
            $this->session->set_flashdata('warning_message',lang('files.invalid_id'));
            redirect(SITE_AREA.'/'.lang('files.slug'));
        }
        else
        {
            if ($this->files_model->delete_permanent($id))
            {
                $this->session->set_flashdata('success_message',lang('files.delete_success'));
            }
            else
            {
                $this->session->set_flashdata('warning_message',$this->files_model->error);
            }

            redirect(SITE_AREA.'/'.lang('files.slug'));
        }
    }

    //--------------------------------------------------------------------


    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    private function save_data($type='insert', $id=0)
    {
        // if ($type == 'update') {
        //     $_POST['id'] = $id;
        // }

        $this->load->library('form_validation');

        if($type == "insert")
        {
            $this->form_validation->set_rules('title','Title','required|trim|is_unique[files.title]|max_length[255]');
        }
        else
        {
            $this->form_validation->set_rules('title','Title','required|trim|max_length[255]');
        }
        $this->form_validation->set_rules('date','Date','required|trim');
        $this->form_validation->set_rules('file','File','trim|max_length[255]');
        $this->form_validation->set_rules('publish','Publish','required|trim|integer|max_length[1]');

        $this->form_validation->set_error_delimiters('<p>', '</p>');

        if($this->form_validation->run() === FALSE)
        {
            return validation_errors();
        }
        else
        {
            $input_date     = explode('-', $this->input->post('date'));
            $date           = $input_date[2].'-'.$input_date[1].'-'.$input_date[0];

            $data = array(
                        'title'         => $this->input->post('title'),
                        'date'          => $date,
                        'slug'          => generate_slug($this->input->post('title')),
                        'publish'       => $this->input->post('publish'),
                    );

            if($this->input->post('file') != "")
            {
                $data['file'] = 'files/'.$this->input->post('file');
            }

            if ($type == 'insert')
            {
                $id = $this->files_model->insert($data);

                if (is_numeric($id))
                {
                    $return = $id;
                }
                else
                {
                    $return = FALSE;
                }
            }
            else if ($type == 'update')
            {
                $return = $this->files_model->update($id, $data);
            }

            return $return;
            // print_r($return);exit();
        }
    }

    //--------------------------------------------------------------------
}