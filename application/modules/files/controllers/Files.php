<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Articles extends Public_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('articles_model');
        $this->lang->load('articles');
    }

	//--------------------------------------------------------------------


	public function index()
	{
		$offsets = $this->uri->segment(2);

		// Pagination setting
		$limit 		= 6;
        if($offsets > 0)
        {
            $offset = ($offsets - 1) * $limit;
        }
        else
        {
            $offset = 0;
        }

		$articles 	= $this->articles_model->order_by('created_on', 'desc')->limit($limit, $offset)->find_all();

		// Pagination
        $this->load->library('pagination');

        $total_articles = $this->articles_model->count_all();

        $total_page = ceil($total_articles / $limit);

        $config['prev_link']            	= FALSE;
        $config['next_link']            	= FALSE;
        $config['first_link']           	= FALSE;
        $config['last_link']            	= FALSE;
        $config['cur_tag_open']         	= '<li class="pagination__item"><a class="pagination__link is-current">';
        $config['cur_tag_close']        	= '</a></li>';
        $config['num_tag_open']         	= '<li class="pagination__item">';
        $config['num_tag_close']        	= '</li>';
        $config['num_links'] 				= 2;
        $config['attributes'] 				= array('class' => 'pagination__link');
        $config['base_url']       			= base_url().lang('module.front_slug');
        $config['total_rows']           	= $total_articles;
        $config['per_page']             	= $limit;
        $config['use_page_numbers']        	= TRUE;
        $config['uri_segment']          	= 2;

        $this->pagination->initialize($config);

        $archive_link   = $config['base_url'];
        $nextprev       = nextprevlink_page($offsets,$limit,$total_articles,$archive_link);
    	$paging         = $this->pagination->create_links();

		$vars = array(
                        'data' 			    => $articles,
                        'nextprev'          => $nextprev,
                        'total_page'        => $total_page
                    );

        // print_r($vars);exit();
        if (!$this->input->is_ajax_request()) {
            // $this->output
            //     ->set_content_type('application/json')
            //     ->set_output(json_encode($vars));

            $this->template->set($vars);
            $this->template->title('Artikel');
    		$this->render('index');
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($vars));
        }
	}

	//--------------------------------------------------------------------


	public function detail_article($slug)
	{
		if($slug == FALSE)
		{
			show_404();
		}

		$detail_article[] = $this->articles_model->find_by('slug', $slug);
		if($detail_article)
		{
			$other_article = $this->articles_model->order_by('created_on', 'desc')->limit(3)->find_all_by(array('publish'=>1, 'id !='=>$detail_article[0]->id));
		}
		else
		{
			show_404();
		}

		$custom_meta_tag = array(
								'title' 		=> $detail_article[0]->title,
								'url' 			=> base_url().lang('module.slug').'/'.$detail_article[0]->slug,
								'image' 		=> config_item('uploads_url').$detail_article[0]->image,
								'description' 	=> str_replace(array('<p>', '</p>'), array('', ''),$detail_article[0]->summary)
							);

		$vars = array(
                        'detail_article' 		=> $detail_article,
                        'other_article' 		=> $other_article,
                        'custom_meta_tag' 	    => $custom_meta_tag,
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title($detail_article[0]->title.' ~ Articles');
		$this->render('detail');
	}

	//--------------------------------------------------------------------


    /*
        Method: articles_section()

        Displays articles section on homepage.
    */
    public function articles_section()
    {
        $article = $this->articles_model->order_by('created_on', 'desc')->limit(1)->find_all();


        $vars = array(
                        'article' => $article
                    );

        $this->load->view('public/include/articles_section', $vars);
    }

    //--------------------------------------------------------------------
}

/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */