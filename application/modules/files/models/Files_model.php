<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Files_model extends MY_Model {

    protected $table_name   = "files";
    protected $key          = "id";
    protected $soft_deletes = true;
    protected $date_format  = "datetime";

    protected $log_user     = FALSE;

    protected $set_created  = true;
    protected $set_modified = true;
    protected $created_field = "created_on";
    protected $modified_field = "modified_on";

    /*
        For performance reasons, you may require your model to NOT return the
        id of the last inserted row as it is a bit of a slow method. This is
        primarily helpful when running big loops over data.
     */
    protected $return_insert_id     = TRUE;

    // The default type of element data is returned as.
    protected $return_type          = "object";

    /*
        You may need to move certain rules (like required) into the
        $insert_validation_rules array and out of the standard validation array.
        That way it is only required during inserts, not updates which may only
        be updating a portion of the data.
     */
    protected $validation_rules         = array();
    protected $insert_validation_rules  = array();
    protected $skip_validation          = FALSE;

    //--------------------------------------------------------------------


    public function find_all()
    {
        $this->db->where('deleted', '0');
        return parent::find_all();
    }

    public function find_all_published()
    {
        $this->db->where('deleted', '0');
        $this->db->where('publish', '1');
        return parent::find_all();
    }

    public function find_all_deleted()
    {
        $this->db->from($this->table_name);
        $this->db->where('deleted', '1');

        $query =  $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return FALSE;
        }
    }

    //--------------------------------------------------------------------


    public function count_all()
    {
        $this->db->where('deleted', '0');
        return parent::count_all();
    }

    public function count_all_published()
    {
        $this->db->where('deleted', '0');
        $this->db->where('publish', '1');
        return parent::count_all();
    }

    public function count_all_deleted()
    {
        $this->db->where('deleted', '1');
        return parent::count_all();
    }

    public function count_other_stories($id)
    {
        if (empty($this->selects))
        {
            $this->db->select($this->table_name .'.*');
        }

        $this->db->from($this->table_name);

        $this->db->where($this->table_name.'.id !=', $id);
        $this->db->where($this->table_name.'.publish', '1');
        $this->db->where($this->table_name.'.deleted', '0');

        $query =  $this->db->get();

        return $query->num_rows();
    }

    //--------------------------------------------------------------------


    public function delete_permanent($id)
    {
        return $this->db->delete($this->table_name, array('id' => $id));
    }

    //--------------------------------------------------------------------


    public function change_publish_status($id, $status)
    {
        return $this->db->update($this->table_name, array('publish' => $status), array('id' => $id));
    }

    public function restore($id)
    {
        return $this->db->update($this->table_name, array('deleted' => 0), array('id' => $id));
    }

}
