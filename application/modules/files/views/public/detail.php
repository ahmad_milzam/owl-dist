<div class="topbar">
  <div class="grid">
    <!-- <div class="grid__item small-1of2 medium-1of4">
      <a href="" class="topbar__link is-active">LIHAT SEMUA</a>
    </div> -->
    <div class="grid__item small-1of2 medium-1of3">
      <a href="<?php echo base_url('artikel'); ?>" class="topbar__link <?php echo $this->uri->segment(1) == 'artikel' ? 'is-active' : ''; ?>">ARTIKEL</a>
    </div>
    <div class="grid__item small-1of2 medium-1of3">
      <a href="<?php echo base_url('tips'); ?>" class="topbar__link <?php echo $this->uri->segment(1) == 'tips' ? 'is-active' : ''; ?>">TIPS</a>
    </div>
    <div class="grid__item small-1 medium-1of3">
      <a href="<?php echo base_url('tweet'); ?>" class="topbar__link <?php echo $this->uri->segment(1) == 'tweet' ? 'is-active' : ''; ?>">TWEET</a>
    </div>
  </div>
</div>
<div class="grid pv+">
  <div class="grid__item small-1 medium-2of3 small-mb medium-mb0">
    <div class="mb">
      <h1 class="mb--">
        <?php echo $detail_article[0]->title; ?>
      </h1>
      <small class="text-disabled">Ditulis tanggal <?php echo date('d M Y', strtotime($detail_article[0]->created_on)); ?></small>
    </div>
    <div class="text-center mb">
      <img src="<?php echo config_item('uploads_url').$detail_article[0]->image; ?>" alt="<?php echo $detail_article[0]->title; ?>">
    </div>
    <?php echo $detail_article[0]->content; ?>
    <ul class="mt list-inline list-inline--middle">
      <li class="list-inline__item">
        <strong>BAGIKAN:</strong>
      </li>
      <li class="list-inline__item">
        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(base_url().'artikel/'.$detail_article[0]->slug); ?>" class="btn btn--small btn--facebook js-shareBtn">
          <i class="icon icon--facebook"></i> Facebook
        </a>
      </li>
      <li class="list-inline__item">
        <a href="https://twitter.com/intent/tweet?text=<?php echo $detail_article[0]->title; ?>&url=<?php echo urlencode(base_url().'artikel/'.$detail_article[0]->slug); ?>" class="btn btn--small btn--twitter js-shareBtn">
          <i class="icon icon--twitter"></i> Twitter
        </a>
      </li>
    </ul>
  </div>
  <div class="grid__item small-1 medium-1of3">
    <div class="h3 underline ph-"><?php echo ucfirst($this->uri->segment(1)) ?> Lainnya</div>
    <ul class="list-ui list-ui--small">
      <?php foreach($other_article AS $article) : ?>
      <li class="list-ui__item">
        <a class="link-primary" href="<?php echo base_url().$this->uri->segment(1).'/'.$article->slug; ?>"><?php echo $article->title; ?></a>
        <br>
        <small class="text-disabled"><?php echo date('d M Y', strtotime($article->created_on)); ?></small>
      </li>
      <?php endforeach; ?>
    </ul>
  </div>
</div>