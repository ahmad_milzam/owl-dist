<div class="topbar">
  <div class="grid">
    <!-- <div class="grid__item small-1of2 medium-1of4">
      <a href="" class="topbar__link is-active">LIHAT SEMUA</a>
    </div> -->
    <div class="grid__item small-1of2 medium-1of3">
      <a href="<?php echo base_url('artikel'); ?>" class="topbar__link <?php echo $this->uri->segment(1) == 'artikel' ? 'is-active' : ''; ?>">ARTIKEL</a>
    </div>
    <div class="grid__item small-1of2 medium-1of3">
      <a href="<?php echo base_url('tips'); ?>" class="topbar__link <?php echo $this->uri->segment(1) == 'tips' ? 'is-active' : ''; ?>">TIPS</a>
    </div>
    <div class="grid__item small-1 medium-1of3">
      <a href="<?php echo base_url('tweet'); ?>" class="topbar__link <?php echo $this->uri->segment(1) == 'tweet' ? 'is-active' : ''; ?>">TWEET</a>
    </div>
  </div>
</div>

<?php if(! empty($data)) : ?>
<div class="article-wrapper">
  <div class="grid pv+">
    <div class="grid__item small-1">
      <ul class="block-list block-list--small-1 block-list--medium-2" id="js-articleContainer" data-type="<?php echo $this->uri->segment(1); ?>" data-totalpage="<?php echo $total_page; ?>" data-currentpage="1">

        <?php foreach($data AS $article) : ?>
        <li class="block-list__item fadeInUp delay">
          <div class="article">
            <div class="article__table">
              <div class="article__cell">
                <small class="article__type">ARTICLE</small>
                <h3 class="article__title">
                  <span class="inline-border"><?php echo $article->title; ?></span>
                </h3>
                <p class="article__desc">
                  <?php echo $article->summary; ?>
                </p>
                <a href="<?php echo base_url().lang('module.front_slug').'/'.$article->slug; ?>" class="btn btn--accent btn--icon">
                  <i class="icon icon--article btn__icon"></i>
                  <span class="btn__text">BACA SELENGKAPNYA</span>
                </a>
              </div>
            </div>
            <div class="article__img" style="background-image: url(<?php echo config_item('uploads_url').$article->image; ?>"></div>
          </div>
        </li>
        <?php endforeach; ?>

      </ul>
      <div class="loading hide" id="js-loading">
        <span class="loading__title">
          Memuat <?php echo $this->uri->segment(1); ?>
        </span>
        <span class="loading__icon">
          <i class="icon icon--spinner icon--larger spining"></i>
        </span>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>