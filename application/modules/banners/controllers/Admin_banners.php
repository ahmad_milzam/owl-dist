<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_banners extends Admin_Controller
{
	function __construct()
    {
        parent::__construct();

        $this->load->model('banners_model');
        $this->lang->load('banners');
    }

	//--------------------------------------------------------------------


    public function index()
    {
        // Breadcrumb setting
        $this->load->library('breadcrumb');

        $this->breadcrumb->home_title('<i class="fa fa-home"></i> Home', 'admin');
        $this->breadcrumb->add('Banners');

        $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);
        // End Breadcrumb setting

        // Pagination setting
        $offsets = $this->input->get('page');
        if($offsets > 0)
        {
            $offset = ($offsets - 1) * $this->data_limit;
        }
        else
        {
            $offset = 0;
        }

        $data_list  = $this->banners_model->order_by('position', 'asc')->limit($this->data_limit, $offset)->find_all();
        $data_trash = $this->banners_model->find_all_deleted();

        $this->load->library('pagination');

        $total_data = $this->banners_model->count_all();

        $this->pager_param['base_url']                = site_url(SITE_AREA.'/'.lang('banners.slug').'?');
        $this->pager_param['total_rows']              = $total_data;
        $this->pager_param['per_page']                = $this->data_limit;
        $this->pager_param['use_page_numbers']        = TRUE;
        $this->pager_param['enable_query_strings']    = TRUE;
        $this->pager_param['page_query_string']       = TRUE;
        $this->pager_param['query_string_segment']    = "page";

        $this->pagination->initialize($this->pager_param);
        // End Pagination setting

        $vars = array(
                        'page'          => $offsets,
                        'limit'         => $this->data_limit,
                        'data_list'     => $data_list,
                        'data_trash'    => $data_trash,
                        'total_data'    => $total_data,
                        'breadcrumb'    => $breadcrumb
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('Banners');
        $this->render('banners_view');
    }

    //--------------------------------------------------------------------


    public function trash()
    {
        // Breadcrumb setting
        $this->load->library('breadcrumb');

        $this->breadcrumb->home_title('<i class="fa fa-banners"></i> Home', 'admin');
        $this->breadcrumb->add('Banners', SITE_AREA.'/'.lang('banners.slug'));
        $this->breadcrumb->add('Banners Trash');

        $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);
        // End Breadcrumb setting

        // Pagination setting
        $offsets = $this->input->get('page');
        if($offsets > 0)
        {
            $offset = ($offsets - 1) * $this->data_limit;
        }
        else
        {
            $offset = 0;
        }

        $data_list  = $this->banners_model->order_by('created_on', 'desc')->limit($this->data_limit, $offset)->find_all_deleted();

        $this->load->library('pagination');

        $total_data = $this->banners_model->count_all_deleted();

        $this->pager_param['base_url']                = site_url(SITE_AREA.'/'.lang('banners.slug').'?');
        $this->pager_param['total_rows']              = $total_data;
        $this->pager_param['per_page']                = $this->data_limit;
        $this->pager_param['use_page_numbers']        = TRUE;
        $this->pager_param['enable_query_strings']    = TRUE;
        $this->pager_param['page_query_string']       = TRUE;
        $this->pager_param['query_string_segment']    = "page";

        $this->pagination->initialize($this->pager_param);
        // End Pagination setting

        $vars = array(
                        'page'          => $offsets,
                        'limit'         => $this->data_limit,
                        'data_list'     => $data_list,
                        'total_data'    => $total_data,
                        'breadcrumb'    => $breadcrumb
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('Banners Trash');
        $this->render('banners_trash_view');
    }

    //--------------------------------------------------------------------


    public function create()
    {
        if ($this->input->post('submit'))
        {
            if (is_numeric($this->save_data()))
            {
                $this->session->set_flashdata('success_message',lang('banners.create_success'));
                redirect(SITE_AREA.'/'.lang('banners.slug'));
            }
            else
            {
                $message = lang('banners.create_failure');
                $this->render(lang('banners.slug').'_create_view');
            }
        }

        $total_data = $this->banners_model->count_all();

        // Breadcrumb setting
        $this->load->library('breadcrumb');

        $this->breadcrumb->home_title('<i class="fa fa-banners"></i> Home', 'admin');
        $this->breadcrumb->add('Banners', SITE_AREA.'/'.lang('banners.slug'));
        $this->breadcrumb->add('Create Banners');
        // End Breadcrumb setting

        $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);

        $vars = array(
                        'message'       => isset($message) ? $message : '',
                        'breadcrumb'    => $breadcrumb,
                        'total_data'    => $total_data
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('Banners - Create Banners');
        $this->render(lang('banners.slug').'_create_view');
    }

    //--------------------------------------------------------------------


    public function edit($id)
    {
        if(empty($id))
        {
            $this->session->set_flashdata('error_message',lang('banners.invalid_id'));
            redirect(SITE_AREA.'/'.lang('banners.slug'));
        }

        if ($this->input->post('submit'))
        {
            // var_dump($this->save_data('update', $id));exit();

            if (is_bool($this->save_data('update', $id)))
            {
                $this->session->set_flashdata('success_message',lang('banners.edit_success'));
                redirect(SITE_AREA.'/'.lang('banners.slug').'/edit/'.$id);
            }
            else
            {
                $message = lang('banners.edit_failure');
                $this->render(lang('banners.slug').'_edit_view');
            }
        }

        $data[]     = $this->banners_model->find($id);
        $total_data = $this->banners_model->count_all();

        // Breadcrumb setting
        $this->load->library('breadcrumb');

        $this->breadcrumb->home_title('<i class="fa fa-banners"></i> Home', 'admin');
        $this->breadcrumb->add('Banners', SITE_AREA.'/'.lang('banners.slug'));
        $this->breadcrumb->add('Edit Banners');
        // End Breadcrumb setting

        $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);

        $vars = array(
                        'data'          => $data,
                        'total_data'    => $total_data,
                        'message'       => isset($message) ? $message : '',
                        'breadcrumb'    => $breadcrumb
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('Banners - Edit Banners');
        $this->render(lang('banners.slug').'_edit_view');
    }

    //--------------------------------------------------------------------


    public function move_trash($id)
    {
        if(empty($id))
        {
            $this->session->set_flashdata('error_message',lang('banners.invalid_id'));
            redirect(SITE_AREA.'/'.lang('banners.slug'));
        }
        else
        {
            if ($this->banners_model->delete($id))
            {
                $this->session->set_flashdata('success_message',lang('banners.trash_success'));
            }
            else
            {
                $this->session->set_flashdata('error_message',$this->banners_model->error);
            }

            redirect(SITE_AREA.'/'.lang('banners.slug'));
        }
    }

    //--------------------------------------------------------------------


    public function restore($id)
    {
        if(empty($id))
        {
            $this->session->set_flashdata('error_message',lang('banners.invalid_id'));
            redirect(SITE_AREA.'/'.lang('banners.slug'));
        }
        else
        {
            if ($this->banners_model->restore($id))
            {
                $this->session->set_flashdata('success_message',lang('banners.restore_success'));
            }
            else
            {
                $this->session->set_flashdata('error_message',$this->banners_model->error);
            }

            redirect(SITE_AREA.'/'.lang('banners.slug'));
        }
    }

    //--------------------------------------------------------------------


    public function delete($id)
    {
        if(empty($id))
        {
            $this->session->set_flashdata('error_message',lang('banners.invalid_id'));
            redirect(SITE_AREA.'/'.lang('banners.slug'));
        }
        else
        {
            if ($this->banners_model->delete_permanent($id))
            {
                $this->session->set_flashdata('success_message',lang('banners.delete_success'));
            }
            else
            {
                $this->session->set_flashdata('error_message',$this->banners_model->error);
            }

            redirect(SITE_AREA.'/'.lang('banners.slug'));
        }
    }

    //--------------------------------------------------------------------


    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    private function save_data($type='insert', $id=0)
    {
        // if ($type == 'update') {
        //     $_POST['id'] = $id;
        // }

        $this->load->library('form_validation');

        if($type == "insert")
        {
            $this->form_validation->set_rules('title','Title','trim|is_unique[banners.title]|max_length[255]');
        }
        else
        {
            $this->form_validation->set_rules('title','Title','trim|max_length[255]');
        }
        $this->form_validation->set_rules('sub_title','Sub Title','trim|max_length[255]');
        $this->form_validation->set_rules('image_alt_text','Image Alt Text','required|trim|max_length[255]');
        $this->form_validation->set_rules('url','URL','trim|max_length[255]');
        $this->form_validation->set_rules('position','Position','required|trim|is_numeric|max_length[11]');
        $this->form_validation->set_rules('publish','Publish','required|trim|integer|max_length[1]');

        $this->form_validation->set_error_delimiters('<p>', '</p>');

        if($this->form_validation->run() === FALSE)
        {
            return validation_errors();
        }
        else
        {
            $data = array(
                        'title'             => $this->input->post('title'),
                        'sub_title'         => $this->input->post('sub_title'),
                        'image_alt_text'    => $this->input->post('image_alt_text'),
                        'url'               => $this->input->post('url'),
                        'position'          => $this->input->post('position'),
                        'slug'              => generate_slug($this->input->post('title')),
                        'publish'           => $this->input->post('publish'),
                    );

            if($this->input->post('image_large') != "")
            {
                $data['image_large'] = 'files/'.$this->input->post('image_large');
            }

            if($this->input->post('image_small') != "")
            {
                $data['image_small'] = 'files/'.$this->input->post('image_small');
            }

            if ($type == 'insert')
            {
                $id = $this->banners_model->insert($data);

                if (is_numeric($id))
                {
                    $return = $id;
                }
                else
                {
                    $return = FALSE;
                }
            }
            else if ($type == 'update')
            {
                $return = $this->banners_model->update($id, $data);
            }


            return $return;
            // print_r($return);exit();
        }
    }

    //--------------------------------------------------------------------
}