<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// General
$lang['banners.name']			            	= 'Banners';
$lang['banners.slug']			            	= 'banners';
$lang['banners.front_slug']			        	= 'banners';

// Page Components
$lang['banners.header_title']			    	= 'Banners';
$lang['banners.content_title']			    	= 'Banners List';
$lang['banners.content_trash_title']			= 'Banners Trash';

$lang['banners.create_success']			    	= 'Banner successfully created.';
$lang['banners.create_failure']			    	= 'There was a problem creating banner.';
$lang['banners.create_button']					= 'Create Banner';

$lang['banners.edit_caption']			    	= 'Edit this banner';
$lang['banners.edit_success']			    	= 'Banner successfully edited.';
$lang['banners.edit_failure']			    	= 'There was a problem editing banner.';
$lang['banners.edit_submit_button']				= 'Edit Banner';
$lang['banners.edit_button']					= 'Edit';

$lang['banners.trash_caption']			    	= 'Move this banner to the Trash';
$lang['banners.trash_success']			    	= 'Banner successfully moved to the trash.';
$lang['banners.trash_failure']			    	= 'There was a problem moving banner to the trash.';
$lang['banners.trash_confirm']					= 'Are you sure you want to move this banner to the trash?';
$lang['banners.trash_button']					= 'Trash';

$lang['banners.restore_caption']			    = 'Restore this banner from the Trash';
$lang['banners.restore_success']			    = 'Banner successfully restored.';
$lang['banners.restore_failure']			    = 'There was a problem restoring banner.';
$lang['banners.restore_confirm']				= 'Are you sure you want to restore this banner?';
$lang['banners.restore_button']					= 'Restore';

$lang['banners.delete_caption']			    	= 'Delete this banner permanently';
$lang['banners.delete_success']			    	= 'Banner successfully deleted.';
$lang['banners.delete_failure']			    	= 'There was a problem deleting banner.';
$lang['banners.delete_confirm']					= 'Are you sure you want to delete this banner permanently?';
$lang['banners.delete_button']					= 'Delete';

$lang['banners.invalid_id']			        	= 'Invalid data ID.';

$lang['banners.content_create_title']			= 'Create Banner';
$lang['banners.content_edit_title']				= 'Edit Banner';

$lang['banners.back_button']					= 'Back';
$lang['banners.cancel_button']					= 'Cancel';
