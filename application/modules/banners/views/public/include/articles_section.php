<?php if(! empty($article)) : ?>
<div class="article article--inline article--big">
  <div class="article__table">
    <div class="article__cell">
      <small>ARTICLE</small>
      <h3 class="article__title">
        <span class="inline-border"><?php echo $article[0]->title; ?></span>
      </h3>
      <p class="article__desc">
        <?php echo $article[0]->summary; ?>
      </p>
      <a href="<?php echo base_url().'artikel/'.$article[0]->slug; ?>" class="btn btn--accent btn--icon">
        <i class="icon icon--article btn__icon"></i>
        <span class="btn__text">BACA SELENGKAPNYA</span>
      </a>
    </div>
  </div>
  <div class="article__img" style="background-image: url(<?php echo config_item('uploads_url').$article[0]->image; ?>"></div>
</div>
<?php endif; ?>