<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
		<?php echo lang('banners.header_title'); ?>
		</h1>
		<?php echo $breadcrumb; ?>
	</section>

	<!-- Main content -->
	<section class="content">

	<?php if($this->session->flashdata('success_message') != "") : ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> Success!</h4>
		<?php echo $this->session->flashdata('success_message'); ?>
	</div>
    <?php endif; ?>

	<?php if($this->session->flashdata('error_message') != "") : ?>
	<div class="alert alert-error alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-ban"></i> Error!</h4>
		<?php echo $this->session->flashdata('error_message'); ?>
	</div>
    <?php endif; ?>

	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title"><?php echo lang('banners.content_title'); ?></h3>
					<div class="box-tools">
						<?php
							if(isset($data_trash) && ! empty($data_trash))
							{
	    						echo anchor(SITE_AREA.'/'.lang('banners.slug').'/trash',
	    												lang('banners.trash_button'),
	    												array('class' => 'btn btn-default btn-sm')
											);
							}
						?>
						<?php
    						echo anchor(SITE_AREA.'/'.lang('banners.slug').'/create',
    												lang('banners.create_button'),
    												array('class' => 'btn btn-primary btn-sm')
										);
    					?>
					</div>
				</div><!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tbody>
							<tr>
								<th width="10%">Position</th>
								<th width="20%">Title</th>
								<th width="30%">Image</th>
								<th width="10%">Publish</th>
								<th width="15%">Created</th>
								<th width="15%">Action</th>
							</tr>
						<?php if(! empty($data_list)) : ?>
							<?php $i=0; foreach($data_list AS $data) : $i++; ?>
							<tr>
								<td>
									<?php
										if($page > 1)
										{
											$page_num = $page - 1;
										}
										else
										{
											$page_num = $page;
										}

										$num = ($limit * $page_num) + $i;
										echo $data->position;
									?>
								</td>
								<td><?php echo $data->title; ?></td>
								<td>
									<img src="<?php echo config_item('uploads_url').$data->image_large; ?>" width="300px">
								</td>
								<td>
									<?php
										switch($data->publish)
										{
											case '0' : $status = '<span class="label label-info">No</span>'; break;
											case '1' : $status = '<span class="label label-success">Yes</span>'; break;
										}
										echo $status;
									?>
								</td>
    							<td><?php echo date('d F Y - H:i',strtotime($data->created_on));?></td>
								<td>
									<?php
			    						echo anchor(SITE_AREA.'/'.lang('banners.slug').'/edit/' . $data->id,
			    												'<i class="fa fa-fw fa-edit"></i> '.lang('banners.edit_button'),
			    												array('class' => 'btn btn-warning btn-sm', 'title' => lang('banners.edit_caption'))
													);
			    					?>
			    					<?php
										echo anchor(SITE_AREA.'/'.lang('banners.slug').'/move_trash/' . $data->id,
																'<i class="fa fa-fw fa-trash-o"></i> '.lang('banners.trash_button'),
																array('class' => 'btn btn-danger btn-sm', 'title' => lang('banners.trash_caption'), "onclick" => "return confirm('".lang("banners.trash_confirm")."')")
			                                        );
			    					?>
								</td>
							</tr>
							<?php endforeach; ?>
						<?php else : ?>
							<tr>
								<td colspan="6">Empty records..</td>
							</tr>
						<?php endif; ?>
						</tbody>
					</table>
				</div><!-- /.box-body -->

				<!-- Pagination -->
				<?php echo $this->pagination->create_links(); ?>
				<!-- End Pagination -->
			</div><!-- /.box -->
		</div>
	</div>

	</section><!-- /.content -->

</div><!-- /.content-wrapper -->