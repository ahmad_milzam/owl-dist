<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
		<?php echo lang('banners.header_title'); ?>
		</h1>
		<?php echo $breadcrumb; ?>
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title"><?php echo lang('banners.content_create_title'); ?></h3>
					</div><!-- /.box-header -->

					<?php echo form_open(); ?>

					<?php
						$error_class 	= 'has-error';
						$error_icon 	= '<i class="fa fa-times-circle-o"></i>';
					?>

						<div class="box-body">
							<?php if(isset($message) && ! empty($message)) : ?>
							<div class="alert alert-danger alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<h4><i class="icon fa fa-ban"></i> Error!</h4>
								<?php echo $message; ?>
							</div>
					    <?php endif; ?>

							<!-- Title -->
							<div class="form-group <?php echo form_error('title') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('title') ? $error_icon : ''; ?> Title*
								</label>
								<input type="text" name="title" class="form-control" value="<?php echo set_value('title'); ?>" required>
								<p class="help-block"><?php echo form_error('title');?></p>
							</div>

							<!-- Sub Title -->
							<div class="form-group <?php echo form_error('sub_title') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('sub_title') ? $error_icon : ''; ?> Sub Title
								</label>
								<input type="text" name="sub_title" class="form-control" value="<?php echo set_value('sub_title'); ?>">
								<p class="help-block"><?php echo form_error('sub_title');?></p>
							</div>

							<!-- Image Large -->
							<div class="form-group <?php echo form_error('image_large') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('image_large') ? $error_icon : ''; ?> Image Large*
								</label>
								<div class="input-group">
									<div class="input-group-btn">
										<a href="<?php echo base_url().'filemanager/dialog.php?type=1&field_id=image_large_path&relative_url=1'; ?>" class="file-iframe-btn" data-fancybox-type="iframe">
											<button type="button" class="btn btn-block btn-default btn-flat">Browse</button>
										</a>
									</div>
									<input id="image_large_path" name="image_large" type="text" class="form-control" value="<?php echo set_value('image_large'); ?>" required>
								</div>
							</div>

							<!-- Image Small -->
							<div class="form-group <?php echo form_error('image_small') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('image_small') ? $error_icon : ''; ?> Image Small*
								</label>
								<div class="input-group">
									<div class="input-group-btn">
										<a href="<?php echo base_url().'filemanager/dialog.php?type=1&field_id=image_small_path&relative_url=1'; ?>" class="file-iframe-btn" data-fancybox-type="iframe">
											<button type="button" class="btn btn-block btn-default btn-flat">Browse</button>
										</a>
									</div>
									<input id="image_small_path" name="image_small" type="text" class="form-control" value="<?php echo set_value('image_small'); ?>" required>
								</div>
							</div>

							<!-- Image Alt Text -->
							<div class="form-group <?php echo form_error('image_alt_text') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('image_alt_text') ? $error_icon : ''; ?> Image Alt Text*
								</label>
								<input type="text" name="image_alt_text" class="form-control" value="<?php echo set_value('image_alt_text'); ?>" required>
								<p class="help-block"><?php echo form_error('image_alt_text');?></p>
							</div>

							<!-- URL -->
							<div class="form-group <?php echo form_error('url') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('url') ? $error_icon : ''; ?> URL
								</label>
								<input type="text" name="url" class="form-control" value="<?php echo set_value('url'); ?>">
								<p class="help-block"><?php echo form_error('url');?></p>
							</div>

							<!-- Position -->
							<div class="form-group <?php echo form_error('position') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('position') ? $error_icon : ''; ?> Position*
								</label>
								<select name="position" class="form-control">
								<?php if($total_data > 0) : ?>
									<?php for($i=1;$i<=$total_data;$i++) : ?>
									<option value="<?php echo $i; ?>" <?php echo set_select('position', $i); ?>><?php echo $i; ?></option>
									<?php endfor; ?>
								<?php else : ?>
									<option value="1" <?php echo set_select('position', '1', TRUE); ?>>1</option>
								<?php endif; ?>
								</select>
								<p class="help-block"><?php echo form_error('position');?></p>
							</div>

							<!-- Publish -->
							<div class="form-group <?php echo form_error('publish') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('publish') ? $error_icon : ''; ?> Publish*
								</label>
								<select name="publish" class="form-control">
									<option value="0" <?php echo set_select('publish', '0', TRUE); ?>>No</option>
									<option value="1" <?php echo set_select('publish', '1'); ?>>Yes</option>
								</select>
								<p class="help-block"><?php echo form_error('publish');?></p>
							</div>

						</div><!-- /.box-body -->

						<div class="box-footer">
							<input type="submit" name="submit" class="btn btn-primary" value="<?php echo lang('banners.create_button') ?>">
							&nbsp;
							<a href="<?php echo base_url().'admin/'.lang('banners.slug'); ?>" class="btn btn-default">
								<?php echo lang('banners.cancel_button') ?>
							</a>
						</div>
					<?php echo form_close();?>
				</div><!-- /.box -->
			</div><!--/.col (right) -->
		</div>

	</section><!-- /.content -->

</div><!-- /.content-wrapper -->