<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// General
$lang['testimonials.name']			            	= 'Testimonials';
$lang['testimonials.slug']			            	= 'testimonials';
$lang['testimonials.front_slug']			        = 'testimonials';

// Page Components
$lang['testimonials.header_title']			    	= 'Testimonials';
$lang['testimonials.content_title']			    	= 'Testimonials List';
$lang['testimonials.content_trash_title']			= 'Testimonials Trash';

$lang['testimonials.create_success']			    = 'Testimonial successfully created.';
$lang['testimonials.create_failure']			    = 'There was a problem creating testimonial.';
$lang['testimonials.create_button']					= 'Create Testimonial';

$lang['testimonials.edit_caption']			    	= 'Edit this testimonial';
$lang['testimonials.edit_success']			    	= 'Testimonial successfully edited.';
$lang['testimonials.edit_failure']			    	= 'There was a problem editing testimonial.';
$lang['testimonials.edit_submit_button']			= 'Edit Testimonial';
$lang['testimonials.edit_button']					= 'Edit';

$lang['testimonials.trash_caption']			    	= 'Move this testimonial to the Trash';
$lang['testimonials.trash_success']			    	= 'Testimonial successfully moved to the trash.';
$lang['testimonials.trash_failure']			    	= 'There was a problem moving testimonial to the trash.';
$lang['testimonials.trash_confirm']					= 'Are you sure you want to move this testimonial to the trash?';
$lang['testimonials.trash_button']					= 'Trash';

$lang['testimonials.restore_caption']			    = 'Restore this testimonial from the Trash';
$lang['testimonials.restore_success']			    = 'Testimonial successfully restored.';
$lang['testimonials.restore_failure']			    = 'There was a problem restoring testimonial.';
$lang['testimonials.restore_confirm']				= 'Are you sure you want to restore this testimonial?';
$lang['testimonials.restore_button']				= 'Restore';

$lang['testimonials.delete_caption']			    = 'Delete this testimonial permanently';
$lang['testimonials.delete_success']			    = 'Testimonial successfully deleted.';
$lang['testimonials.delete_failure']			    = 'There was a problem deleting testimonial.';
$lang['testimonials.delete_confirm']				= 'Are you sure you want to delete this testimonial permanently?';
$lang['testimonials.delete_button']					= 'Delete';

$lang['testimonials.invalid_id']			        = 'Invalid data ID.';

$lang['testimonials.content_create_title']			= 'Create Testimonial';
$lang['testimonials.content_edit_title']			= 'Edit Testimonial';

$lang['testimonials.back_button']					= 'Back';
$lang['testimonials.cancel_button']					= 'Cancel';
