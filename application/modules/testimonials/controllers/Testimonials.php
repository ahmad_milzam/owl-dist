<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testimonials extends Public_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('testimonials_model');
        $this->lang->load('testimonials');
    }

	//--------------------------------------------------------------------


	public function index()
	{
		$offsets = $this->uri->segment(2);

		// Pagination setting
		$limit 		= 10;
        if($offsets > 0)
        {
            $offset = ($offsets - 1) * $limit;
        }
        else
        {
            $offset = 0;
        }

		$testimonials 	= $this->testimonials_model->order_by('created_on', 'desc')->limit($limit, $offset)->find_all_published();

		// Pagination
        $this->load->library('pagination');

        $total_testimonials = $this->testimonials_model->count_all_published();

        $total_page = ceil($total_testimonials / $limit);

        $config['prev_link']            	= FALSE;
        $config['next_link']            	= FALSE;
        $config['first_link']           	= FALSE;
        $config['last_link']            	= FALSE;
        $config['cur_tag_open']         	= '<li class="pagination__item"><a class="pagination__link is-current">';
        $config['cur_tag_close']        	= '</a></li>';
        $config['num_tag_open']         	= '<li class="pagination__item">';
        $config['num_tag_close']        	= '</li>';
        $config['num_links'] 				= 2;
        $config['attributes'] 				= array('class' => 'pagination__link');
        $config['base_url']       			= base_url().lang('testimonials.front_slug');
        $config['total_rows']           	= $total_testimonials;
        $config['per_page']             	= $limit;
        $config['use_page_numbers']        	= TRUE;
        $config['uri_segment']          	= 2;

        $this->pagination->initialize($config);

        $archive_link   = $config['base_url'];
        $nextprev       = nextprevlink_page($offsets,$limit,$total_testimonials,$archive_link);
    	$paging         = $this->pagination->create_links();

		$vars = array(
                        'data' 			    => $testimonials,
                        'nextprev'          => $nextprev,
                        'paging'            => $paging
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('Testimonials');
        $this->render('index');
	}

	//--------------------------------------------------------------------
}

/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */