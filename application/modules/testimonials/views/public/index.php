<!-- page content -->
<div class="section">
  <div class="grid">
    <div class="grid__item small-1">
      <h1 class="heading">Testimonials</h1>
    </div>
  </div>
  <?php if(! empty($data)) : ?>
  <div class="grid">
    <?php foreach($data AS $testimonial) : ?>
    <div class="grid__item small-1 medium-1of2">
      <div class="videoflex videoflex--wide">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $testimonial->video_id; ?>" frameborder="0" allowfullscreen></iframe>
      </div>
    </div>
    <?php endforeach; ?>
  </div>
  <?php endif; ?>
</div>
<!-- page content -->

<?php
  if(isset($nextprev['prev_link']))
  {
    $prev_tag_open='<li class="pagination__item"><a href="'.$nextprev['prev_link'].'" class="pagination__link is-button">Prev';
    $prev_tag_close='</a></li>';
  }
  elseif(!isset($nextprev['prev_link']))
  {
    $prev_tag_open='<li class="pagination__item"><a class="pagination__link is-button is-disabled">Prev';
    $prev_tag_close='</a></li>';
  }

  if(isset($nextprev['next_link']))
  {
    $next_tag_open='<li class="pagination__item"><a href="'.$nextprev['next_link'].'" class="pagination__link is-button">Next';
    $next_tag_close='</a></li>';
  }
  elseif(!isset($nextprev['next_link']))
  {
    $next_tag_open='<li class="pagination__item"><a class="pagination__link is-button is-disabled">Next';
    $next_tag_close='</a></li>';
  }
?>

<?php if(!empty($nextprev)) : ?>
<!-- pagination -->
<div class="section">
  <div class="grid">
    <div class="grid__item small-1">
      <ul class="pagination pagination--seperate pagination--responsive">
        <?php echo $prev_tag_open; ?><?php echo $prev_tag_close; ?>
        <?php echo $paging; ?>
        <?php echo $next_tag_open; ?><?php echo $next_tag_close; ?>
      </ul>
    </div>
  </div>
</div>
<!-- pagination -->
<?php endif; ?>