<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
		<?php echo lang('testimonials.header_title'); ?>
		</h1>
		<?php echo $breadcrumb; ?>
	</section>

	<!-- Main content -->
	<section class="content">

	<?php if($this->session->flashdata('success_message') != "") : ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> Success!</h4>
		<?php echo $this->session->flashdata('success_message'); ?>
	</div>
    <?php endif; ?>

	<?php if($this->session->flashdata('warning_message') != "") : ?>
	<div class="alert alert-warning alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-warning"></i> Warning!</h4>
		<?php echo $this->session->flashdata('warning_message'); ?>
	</div>
    <?php endif; ?>

	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title"><?php echo lang('testimonials.content_trash_title'); ?></h3>
					<div class="box-tools">
						<?php
							echo anchor(SITE_AREA.'/'.lang('testimonials.slug'),
    												lang('testimonials.back_button'),
    												array('class' => 'btn btn-primary btn-sm')
										);
						?>
					</div>
				</div><!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tbody>
							<tr>
								<th width="5%">No.</th>
								<th width="20%">Title</th>
								<th width="40%">Video</th>
								<th width="10%">Publish</th>
								<th width="10%">Created</th>
								<th width="15%">Action</th>
							</tr>
						<?php if(! empty($data_list)) : ?>
							<?php $i=0; foreach($data_list AS $data) : $i++; ?>
							<tr>
								<td>
									<?php
										if($page > 1)
										{
											$page_num = $page - 1;
										}
										else
										{
											$page_num = $page;
										}

										$num = ($limit * $page_num) + $i;
										echo $num;
									?>
								</td>
								<td><?php echo $data->title; ?></td>
								<td>
									<iframe width="350" height="250" src="https://www.youtube.com/embed/<?php echo $data->video_id; ?>" frameborder="0" allowfullscreen></iframe>
								</td>
								<td>
									<?php
										switch($data->publish)
										{
											case '0' : $status = '<span class="label label-info">No</span>'; break;
											case '1' : $status = '<span class="label label-success">Yes</span>'; break;
										}
										echo $status;
									?>
								</td>
    							<td><?php echo date('d F Y - H:i',strtotime($data->created_on));?></td>
								<td>
									<?php
			    						echo anchor(SITE_AREA.'/'.lang('testimonials.slug').'/restore/' . $data->id,
			    												'<i class="fa fa-fw fa-refresh"></i> '.lang('testimonials.restore_button'),
			    												array('class' => 'btn btn-warning btn-sm', 'title' => lang('testimonials.restore_caption'), "onclick" => "return confirm('".lang("testimonials.restore_confirm")."')")
													);
			    					?>
			    					<br>
			    					<?php
										echo anchor(SITE_AREA.'/'.lang('testimonials.slug').'/delete/' . $data->id,
																'<i class="fa fa-fw fa-remove"></i> '.lang('testimonials.delete_button'),
																array('class' => 'btn btn-danger btn-sm', 'title' => lang('testimonials.delete_caption'), "onclick" => "return confirm('".lang("testimonials.delete_confirm")."')", "style" => "margin-top: 5px;")
			                                        );
			    					?>
								</td>
							</tr>
							<?php endforeach; ?>
						<?php else : ?>
							<tr>
								<td colspan="6">Empty records..</td>
							</tr>
						<?php endif; ?>
						</tbody>
					</table>
				</div><!-- /.box-body -->

				<!-- Pagination -->
				<?php echo $this->pagination->create_links(); ?>
				<!-- End Pagination -->
			</div><!-- /.box -->
		</div>
	</div>

	</section><!-- /.content -->

</div><!-- /.content-wrapper -->