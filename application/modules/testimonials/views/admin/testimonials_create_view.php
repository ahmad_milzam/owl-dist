<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
		<?php echo lang('testimonials.header_title'); ?>
		</h1>
		<?php echo $breadcrumb; ?>
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title"><?php echo lang('testimonials.content_create_title'); ?></h3>
					</div><!-- /.box-header -->

					<?php echo form_open(); ?>

					<?php
						$error_class 	= 'has-error';
						$error_icon 	= '<i class="fa fa-times-circle-o"></i>';
					?>

						<div class="box-body">
							<?php if(isset($message) && ! empty($message)) : ?>
							<div class="alert alert-danger alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<h4><i class="icon fa fa-ban"></i> Error!</h4>
								<?php echo $message; ?>
							</div>
						    <?php endif; ?>

							<!-- Title -->
							<div class="form-group <?php echo form_error('title') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('title') ? $error_icon : ''; ?> Title*
								</label>
								<input type="text" name="title" class="form-control" value="<?php echo set_value('title'); ?>" required>
								<p class="help-block"><?php echo form_error('title');?></p>
							</div>

							<!-- Video URL -->
							<div class="form-group <?php echo form_error('video_url') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('video_url') ? $error_icon : ''; ?> Video URL*
								</label>
								<input type="text" name="video_url" class="form-control" value="<?php echo set_value('video_url'); ?>" placeholder="ex: https://www.youtube.com/watch?v=4Nv4YcrtTl8" required>
								<p class="help-block"><?php echo form_error('video_url');?></p>
							</div>

							<!-- Publish -->
							<div class="form-group <?php echo form_error('publish') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('publish') ? $error_icon : ''; ?> Publish*
								</label>
								<select name="publish" class="form-control">
									<option value="0" <?php echo set_select('publish', '0', TRUE); ?>>No</option>
									<option value="1" <?php echo set_select('publish', '1'); ?>>Yes</option>
								</select>
								<p class="help-block"><?php echo form_error('publish');?></p>
							</div>

						</div><!-- /.box-body -->

						<div class="box-footer">
							<input type="submit" name="submit" class="btn btn-primary" value="<?php echo lang('testimonials.create_button') ?>">
							&nbsp;
							<a href="<?php echo base_url().'admin/'.lang('testimonials.slug'); ?>" class="btn btn-default">
								<?php echo lang('testimonials.cancel_button') ?>
							</a>
						</div>
					<?php echo form_close();?>
				</div><!-- /.box -->
			</div><!--/.col (right) -->
		</div>

	</section><!-- /.content -->

</div><!-- /.content-wrapper -->