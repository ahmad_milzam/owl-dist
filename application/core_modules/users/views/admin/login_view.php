<div class="login-logo">
    <a href="<?php echo base_url('admin'); ?>">
        <img src="<?php echo config_item('assets_url').'admin/img/logo.png'; ?>" style="height: 100px;">
    </a>
</div><!-- /.login-logo -->

<div class="login-box-body">

    <p class="login-box-msg">Sign in to start your session</p>

    <?php if($this->session->flashdata('error_message') != "") : ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('error_message'); ?>
    </div>
    <?php endif; ?>

    <?php echo form_open(); ?>
        <div class="form-group has-feedback">
            <input type="email" name="email" class="form-control" placeholder="Email" required/>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Password" required/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck">
                    <label>
                        <input type="checkbox" name="remember" value="1" /> Remember Me
                    </label>
                </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
        </div>
    <?php echo form_close(); ?>

</div><!-- /.login-box-body -->