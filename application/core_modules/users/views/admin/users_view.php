<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
		Users
		</h1>
		<?php echo $breadcrumb; ?>
	</section>

	<!-- Main content -->
	<section class="content">

	<?php if($this->session->flashdata('success_message') != "") : ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> Success!</h4>
		<?php echo $this->session->flashdata('success_message'); ?>
	</div>
    <?php endif; ?>

	<?php if($this->session->flashdata('warning_message') != "") : ?>
	<div class="alert alert-warning alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-warning"></i> Warning!</h4>
		<?php echo $this->session->flashdata('warning_message'); ?>
	</div>
    <?php endif; ?>

	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">User List</h3>
					<div class="box-tools">
						<a href="<?php echo base_url('admin/users/create'); ?>">
							<button class="btn btn-block btn-primary btn-sm">Create User</button>
						</a>
					</div>
				</div><!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tbody>
							<tr>
								<th>ID</th>
								<th>Username</th>
								<th>Display Name</th>
								<th>Email</th>
								<th>Role</th>
								<th>Last Login</th>
							</tr>
						<?php if(! empty($users)) : ?>
							<?php foreach($users AS $user) : ?>
							<tr>
								<td><?php echo $user->id; ?></td>
								<td>
									<a href="<?php echo base_url().'admin/users/edit/'.$user->id; ?>">
										<?php echo $user->username; ?>
									</a>
								</td>
								<td><?php echo $user->first_name.' '.$user->last_name; ?></td>
								<td><?php echo $user->email; ?></td>
								<td><?php echo ucfirst($user->user_group_name); ?></td>
								<td>
									<?php echo $user->last_login != "" ? date('d F Y - H:i:s', $user->last_login) : "-"; ?>
								</td>
							</tr>
							<?php endforeach; ?>
						<?php else : ?>
							<tr>
								<td colspan="5">Empty records..</td>
							</tr>
						<?php endif; ?>
						</tbody>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>

	</section><!-- /.content -->

</div><!-- /.content-wrapper -->