<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
		Profile
		</h1>
		<?php echo $breadcrumb; ?>
	</section>

	<!-- Main content -->
	<section class="content">

	<?php if(isset($message) && ! empty($message)) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-ban"></i> Error!</h4>
		<?php echo $message; ?>
	</div>
    <?php endif; ?>

	<?php if($this->session->flashdata('success_message') != "") : ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> Success!</h4>
		<?php echo $this->session->flashdata('success_message'); ?>
	</div>
    <?php endif; ?>

		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">Edit Profile</h3>
					</div><!-- /.box-header -->

					<?php echo form_open();?>

					<?php
						$error_class 	= 'has-error';
						$error_icon 	= '<i class="fa fa-times-circle-o"></i>';
					?>

						<div class="box-body">
							<!-- First Name -->
							<div class="form-group <?php echo form_error('first_name') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('first_name') ? $error_icon : ''; ?> First Name
								</label>
								<input type="text" name="first_name" class="form-control" value="<?php echo $user[0]->first_name; ?>">
								<p class="help-block"><?php echo form_error('first_name');?></p>
							</div>

							<!-- Last Name -->
							<div class="form-group <?php echo form_error('last_name') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('last_name') ? $error_icon : ''; ?> Last Name
								</label>
								<input type="text" name="last_name" class="form-control" value="<?php echo $user[0]->last_name; ?>">
								<p class="help-block"><?php echo form_error('last_name');?></p>
							</div>

							<!-- Username -->
							<div class="form-group <?php echo form_error('username') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('username') ? $error_icon : ''; ?> Username*
								</label>
								<input type="text" name="username" class="form-control" value="<?php echo $user[0]->username; ?>" required>
								<p class="help-block"><?php echo form_error('username');?></p>
							</div>

							<!-- Email -->
							<div class="form-group <?php echo form_error('email') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('email') ? $error_icon : ''; ?> Email*
								</label>
								<input type="email" name="email" class="form-control" value="<?php echo $user[0]->email; ?>" required>
								<p class="help-block"><?php echo form_error('email');?></p>
							</div>

							<!-- Company -->
							<div class="form-group <?php echo form_error('company') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('company') ? $error_icon : ''; ?> Company
								</label>
								<input type="text" name="company" class="form-control" value="<?php echo $user[0]->company; ?>">
								<p class="help-block"><?php echo form_error('company');?></p>
							</div>

							<!-- Phone -->
							<div class="form-group <?php echo form_error('phone') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('phone') ? $error_icon : ''; ?> Phone
								</label>
								<input type="text" name="phone" class="form-control" value="<?php echo $user[0]->phone; ?>">
								<p class="help-block"><?php echo form_error('phone');?></p>
							</div>

							<!-- Password -->
							<div class="form-group <?php echo form_error('password') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('password') ? $error_icon : ''; ?> Password
								</label>
								<input type="password" name="password" class="form-control">
								<p class="help-block"><?php echo form_error('password');?></p>
							</div>

							<!-- Confirm Password -->
							<div class="form-group <?php echo form_error('password_confirm') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('password_confirm') ? $error_icon : ''; ?> Confirm Password
								</label>
								<input type="password" name="password_confirm" class="form-control">
								<p class="help-block"><?php echo form_error('password_confirm');?></p>
							</div>
						</div><!-- /.box-body -->

						<div class="box-footer">
							<input type="submit" name="submit" class="btn btn-primary" value="Edit User">
							&nbsp;
							<a href="<?php echo base_url('admin'); ?>" class="btn btn-default">
								Cancel
							</a>
						</div>
					<?php echo form_close();?>
				</div><!-- /.box -->
			</div><!--/.col (right) -->
		</div>

	</section><!-- /.content -->

</div><!-- /.content-wrapper -->