<li class="dropdown user user-menu">
    <!-- Menu Toggle Button -->
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <img src="<?php echo $profpic; ?>" class="user-image" alt="User Image">
        <!-- hidden-xs hides the username on small devices so only the image appears. -->
        <span class="hidden-xs"><?php echo $current_user->first_name.' '.$current_user->last_name; ?></span>
    </a>
    <ul class="dropdown-menu">
        <!-- The user image in the menu -->
        <li class="user-header">
            <img src="<?php echo $profpic; ?>" class="img-circle" alt="User Image" />
            <p>
                <?php echo $current_user->first_name.' '.$current_user->last_name; ?>
                <small><?php echo $current_user->email; ?></small>
            </p>
        </li>
        <!-- Menu Footer-->
        <li class="user-footer">
            <div class="pull-left">
                <a href="<?php echo base_url('admin/users/profile') ?>" class="btn btn-default btn-flat">Profile</a>
            </div>
            <div class="pull-right">
                <a href="<?php echo base_url('logout') ?>" class="btn btn-default btn-flat">Sign out</a>
            </div>
        </li>
    </ul>
</li>