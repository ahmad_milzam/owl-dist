<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_users extends Admin_Controller
{
	function __construct()
    {
        parent::__construct();

        if(!$this->ion_auth->in_group(array('admin', 'editor')))
        {
            $this->session->set_flashdata('error_message','You are not allowed to visit this page');
            redirect('logout');
        }
    }

	//--------------------------------------------------------------------


    public function index()
    {
        if(!$this->ion_auth->in_group('admin'))
        {
            $this->session->set_flashdata('access_error_message','You are not allowed to visit this page');
            redirect('admin');
        }

        $users = $this->ion_auth->users()->result();
        foreach($users AS $user)
        {
            if($check_group = $this->ion_auth->get_users_groups($user->id)->result())
            {
                foreach($check_group as $group)
                {
                    $user->user_group_id      = $group->id;
                    $user->user_group_name    = $group->name;
                }
            }
        }

        // Breadcrumb setting
        $this->load->library('breadcrumb');

        $this->breadcrumb->home_title('<i class="fa fa-home"></i> Home', 'admin');
        $this->breadcrumb->add('Users');
        // End Breadcrumb setting

        $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);

        $vars = array(
                        'users'        	=> $users,
                        'breadcrumb'    => $breadcrumb
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('Users');
        $this->render('users_view');
    }

    //--------------------------------------------------------------------


    public function create()
    {
    	$groups = $this->ion_auth->groups()->result();

        if ($this->input->post('submit'))
        {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('first_name','First Name','trim|max_length[50]');
			$this->form_validation->set_rules('last_name','Last Name','trim|max_length[50]');
			$this->form_validation->set_rules('company','Company','trim|max_length[100]');
			$this->form_validation->set_rules('phone','Phone','trim|integer|max_length[20]');
			$this->form_validation->set_rules('username','Username','required|trim|max_length[100]|is_unique[users.username]');
			$this->form_validation->set_rules('email','Email','required|trim|valid_email|max_length[100]|is_unique[users.email]');
			$this->form_validation->set_rules('password','Password','required');
			$this->form_validation->set_rules('password_confirm','Password Confirmation','required|matches[password]');
			$this->form_validation->set_rules('group','Group','required|integer');

            $this->form_validation->set_error_delimiters('', '');

            if($this->form_validation->run() === FALSE)
            {
                $this->render('users_create_view');
            }
            else
            {
                $username 		= $this->input->post('username');
				$email 			= $this->input->post('email');
				$password 		= $this->input->post('password');
				$group_id[] 	= $this->input->post('group');

				$additional_data = array(
										'first_name' 	=> $this->input->post('first_name'),
										'last_name' 	=> $this->input->post('last_name'),
										'company' 		=> $this->input->post('company'),
										'phone' 		=> $this->input->post('phone')
									);
				$this->ion_auth->register($username, $password, $email, $additional_data, $group_id);

				$this->session->set_flashdata('success_message',$this->ion_auth->messages());
				redirect('admin/users');
            }
        }

        // Breadcrumb setting
        $this->load->library('breadcrumb');

        $this->breadcrumb->home_title('<i class="fa fa-home"></i> Home', 'admin');
        $this->breadcrumb->add('Users', 'admin/users');
        $this->breadcrumb->add('Create User');
        // End Breadcrumb setting

        $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);

        $vars = array(
                        'groups'    	=> $groups,
                        'breadcrumb'    => $breadcrumb
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('Users - Create User');
        $this->render('users_create_view');
    }

    //--------------------------------------------------------------------


    public function edit($user_id)
    {
        $check_user = $this->ion_auth->user((int) $user_id)->row();
        if(empty($user_id) || ! $check_user)
        {
            $this->session->set_flashdata('warning_message','Invalid user ID');
            redirect('admin/users');
        }
        else
        {
        	$groups = $this->ion_auth->groups()->result();

            if($check_group = $this->ion_auth->get_users_groups($check_user->id)->result())
			{
				foreach($check_group as $group)
				{
                    $check_user->user_group_id      = $group->id;
                    $check_user->user_group_name    = $group->name;
				}
			}

            $user[] = $check_user;
        }

        if ($this->input->post('submit'))
        {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('first_name','First Name','trim|max_length[50]');
			$this->form_validation->set_rules('last_name','Last Name','trim|max_length[50]');
			$this->form_validation->set_rules('company','Company','trim|max_length[100]');
			$this->form_validation->set_rules('phone','Phone','trim|integer|max_length[20]');
			$this->form_validation->set_rules('password','Password', 'trim');
			$this->form_validation->set_rules('password_confirm','Password Confirmation', 'trim|matches[password]');
			$this->form_validation->set_rules('group','Group','required|integer');
			$this->form_validation->set_rules('user_id','User ID','required|trim|integer');

			if($this->input->post('username') == $check_user->username)
			{
				$this->form_validation->set_rules('username','Username','required|trim|max_length[100]');
			}
			else
			{
				$this->form_validation->set_rules('username','Username','required|trim|max_length[100]|is_unique[users.username]');
			}

			if($this->input->post('email') == $check_user->email)
			{
				$this->form_validation->set_rules('email','Email','required|trim|valid_email|max_length[100]');
			}
			else
			{
				$this->form_validation->set_rules('email','Email','required|trim|valid_email|max_length[100]|is_unique[users.email]');
			}


            $this->form_validation->set_error_delimiters('', '');

            if($this->form_validation->run() === FALSE)
            {
                $message = "There was a problem editing profile.";
                $this->render('users_edit_view');
            }
            else
            {
                $user_id = $this->input->post('user_id');

				$new_data = array(
								'username' 		=> $this->input->post('username'),
								'email' 		=> $this->input->post('email'),
								'first_name' 	=> $this->input->post('first_name'),
								'last_name' 	=> $this->input->post('last_name'),
								'company' 		=> $this->input->post('company'),
								'phone' 		=> $this->input->post('phone')
							);

				if(strlen($this->input->post('password'))>=5) $new_data['password'] = $this->input->post('password');

				$this->ion_auth->update($user_id, $new_data);

				//Update the groups user belongs to
				$group = $this->input->post('group');
				if (isset($group) && !empty($group))
				{
					$this->ion_auth->remove_from_group('', $user_id);
					$this->ion_auth->add_to_group($group, $user_id);
				}

				$this->session->set_flashdata('success_message',$this->ion_auth->messages());
                redirect('admin/users/edit/'.$user_id);
            }
        }

        // Breadcrumb setting
        $this->load->library('breadcrumb');

        $this->breadcrumb->home_title('<i class="fa fa-home"></i> Home', 'admin');
        $this->breadcrumb->add('Users', 'admin/users');
        $this->breadcrumb->add('Edit User');
        // End Breadcrumb setting

        $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);

        $vars = array(
                        'groups'        => $groups,
                        'user'          => $user,
                        'breadcrumb'    => $breadcrumb,
                        'message'       => isset($message) ? $message : ''
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('Users - Edit User');
        $this->render('users_edit_view');
    }

    //--------------------------------------------------------------------


    public function delete($user_id)
    {
        if(is_null($user_id))
        {
            $this->session->set_flashdata('message','There\'s no user to delete');
        }
        else
        {
            $this->ion_auth->delete_user($user_id);
            $this->session->set_flashdata('success_message',$this->ion_auth->messages());
        }
        redirect('admin/users');
    }

    //--------------------------------------------------------------------


    public function profile()
    {
        $user[] = $this->current_user;

        if ($this->input->post('submit'))
        {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('first_name','First Name','trim|max_length[50]');
            $this->form_validation->set_rules('last_name','Last Name','trim|max_length[50]');
            $this->form_validation->set_rules('company','Company','trim|max_length[100]');
            $this->form_validation->set_rules('phone','Phone','trim|integer|max_length[20]');
            $this->form_validation->set_rules('password','Password', 'trim');
            $this->form_validation->set_rules('password_confirm','Password Confirmation', 'trim|matches[password]');

            if($this->input->post('username') == $this->current_user->username)
            {
                $this->form_validation->set_rules('username','Username','required|trim|max_length[100]');
            }
            else
            {
                $this->form_validation->set_rules('username','Username','required|trim|max_length[100]|is_unique[users.username]');
            }

            if($this->input->post('email') == $this->current_user->email)
            {
                $this->form_validation->set_rules('email','Email','required|trim|valid_email|max_length[100]');
            }
            else
            {
                $this->form_validation->set_rules('email','Email','required|trim|valid_email|max_length[100]|is_unique[users.email]');
            }


            $this->form_validation->set_error_delimiters('', '');

            if($this->form_validation->run() === FALSE)
            {
                $message = "There was a problem updating profile.";
                $this->render('profile_view');
            }
            else
            {
                $new_data = array(
                                'username'      => $this->input->post('username'),
                                'email'         => $this->input->post('email'),
                                'first_name'    => $this->input->post('first_name'),
                                'last_name'     => $this->input->post('last_name'),
                                'company'       => $this->input->post('company'),
                                'phone'         => $this->input->post('phone')
                            );

                if(strlen($this->input->post('password'))>=5) $new_data['password'] = $this->input->post('password');

                $this->ion_auth->update($this->current_user->id, $new_data);

                $this->session->set_flashdata('success_message',$this->ion_auth->messages());
                redirect('admin/users/profile');
            }
        }

        // Breadcrumb setting
        $this->load->library('breadcrumb');

        $this->breadcrumb->home_title('<i class="fa fa-home"></i> Home', 'admin');
        $this->breadcrumb->add('Profile');
        // End Breadcrumb setting

        $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);

        $vars = array(
                        'user'          => $user,
                        'breadcrumb'    => $breadcrumb,
                        'message'       => isset($message) ? $message : ''
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('Profile');
        $this->render('profile_view');
    }

    //--------------------------------------------------------------------


    public function header_profile()
    {
        $this->load->library('gravatar');

        $gravatar_profile   = $this->gravatar->get_profile_data($this->current_user->email);
        $last_error         = $this->gravatar->last_error();

        if($last_error == 1)
        {
            $profpic = $this->gravatar->get($this->current_user->email);
        }
        else
        {
            $profpic = config_item('assets_url').'admin/img/avatar.png';
        }

        $vars = array(
                        'current_user'  => $this->current_user,
                        'profpic'       => $profpic,
                    );

        // print_r($last_error);exit();

        $this->load->view('admin/header_profile_view', $vars);
    }

    //--------------------------------------------------------------------
}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */