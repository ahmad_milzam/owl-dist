<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends Authenticated_Controller {

	function __construct()
	{
		parent::__construct();

        $this->load->library('users/ion_auth');
	}

	//--------------------------------------------------------------------


	public function index()
	{
        print_r("user list");
	}


	//--------------------------------------------------------------------


	public function login()
	{
        if (!$this->ion_auth->logged_in())
        {
            // $this->session->set_flashdata('message','You must be logged in to view that page.');
        }
        else
        {
        	redirect('admin');
        }

        if ($this->ion_auth->logged_in() === FALSE)
        {
        	if ($this->input->post())
			{
	            $this->load->library('form_validation');

	        	$this->form_validation->set_rules('email','Email','required|trim|max_length[255]');
		        $this->form_validation->set_rules('password','Password','required|trim');
		    	$this->form_validation->set_rules('remember','Remember me','integer');

		        $this->form_validation->set_error_delimiters('', '');

		        if($this->form_validation->run() === TRUE)
				{
					$remember = (bool) $this->input->post('remember');
					if ($this->ion_auth->login($this->input->post('email'), $this->input->post('password'), $remember))
					{
						redirect('admin');
					}
					else
					{
						$this->session->set_flashdata('error_message',$this->ion_auth->errors());
	        			redirect('login');
					}
				}
			}

			$this->template->title('Login');
			$this->render('login_view');
        }
        else
        {
        	$this->session->set_flashdata('message','You must be logged in to view that page.');
        	redirect('admin/login');
        }
	}

	//--------------------------------------------------------------------


	public function logout()
	{
		$this->ion_auth->logout();
  		redirect('login');
	}

}