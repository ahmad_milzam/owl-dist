<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_settings extends Admin_Controller
{
	function __construct()
    {
        parent::__construct();

        if(!$this->ion_auth->in_group(array('admin', 'editor')))
        {
            $this->session->set_flashdata('access_error_message','You are not allowed to visit this page');
            redirect('admin');
        }
    }

	//--------------------------------------------------------------------


    public function index()
    {
        $this->load->model('settings_model');

        $settings_data = "";

        $settings = $this->settings_lib->find_all();

        if ($this->input->post('submit'))
        {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('site_name','Site Name','required|trim|max_length[50]');
            $this->form_validation->set_rules('site_company_name','Company Name','required|trim|max_length[50]');
            $this->form_validation->set_rules('site_email','Email','required|trim|valid_email|max_length[50]');
            $this->form_validation->set_rules('site_address','Address','required|trim|max_length[255]');
            $this->form_validation->set_rules('site_phone','Phone','required|trim|max_length[20]');
            $this->form_validation->set_rules('site_mobile_phone','Mobile Phone','required|trim|max_length[20]');
            $this->form_validation->set_rules('site_facebook_link','Facebook Link','required|trim|max_length[255]');
            $this->form_validation->set_rules('site_twitter_link','Twitter Link','required|trim|max_length[255]');
            $this->form_validation->set_rules('site_instagram_link','Instagram Link','required|trim|max_length[255]');
            $this->form_validation->set_rules('site_gplus_link','Google Plus Link','required|trim|max_length[255]');
            $this->form_validation->set_rules('site_youtube_link','Youtube Link','required|trim|max_length[255]');
            $this->form_validation->set_rules('site_meta_title','Meta Title','required|trim|max_length[255]');
            $this->form_validation->set_rules('site_meta_site_name','Meta Site Name','required|trim|max_length[255]');
            $this->form_validation->set_rules('site_meta_description','Meta Description','required|trim|max_length[255]');
            $this->form_validation->set_rules('site_meta_image','Meta Image','trim|max_length[255]');
            $this->form_validation->set_rules('site_status','Status','trim|integer|max_length[1]');
            $this->form_validation->set_rules('site_page_limit','Page Limit','required|trim|integer|max_length[5]');

            $this->form_validation->set_error_delimiters('', '');

            if($this->form_validation->run() === FALSE)
            {
                $this->session->set_flashdata('error_message','There was a problem saving your settings');
                $this->render('settings_view');
            }
            else
            {
                $settings_data = array(
                                    array('name' => 'site_name', 'value' => $this->input->post('site_name')),
                                    array('name' => 'site_company_name', 'value' => $this->input->post('site_company_name')),
                                    array('name' => 'site_email', 'value' => $this->input->post('site_email')),
                                    array('name' => 'site_address', 'value' => $this->input->post('site_address')),
                                    array('name' => 'site_phone', 'value' => $this->input->post('site_phone')),
                                    array('name' => 'site_mobile_phone', 'value' => $this->input->post('site_mobile_phone')),
                                    array('name' => 'site_facebook_link', 'value' => $this->input->post('site_facebook_link')),
                                    array('name' => 'site_twitter_link', 'value' => $this->input->post('site_twitter_link')),
                                    array('name' => 'site_instagram_link', 'value' => $this->input->post('site_instagram_link')),
                                    array('name' => 'site_gplus_link', 'value' => $this->input->post('site_gplus_link')),
                                    array('name' => 'site_youtube_link', 'value' => $this->input->post('site_youtube_link')),
                                    array('name' => 'site_meta_title', 'value' => $this->input->post('site_meta_title')),
                                    array('name' => 'site_meta_site_name', 'value' => $this->input->post('site_meta_site_name')),
                                    array('name' => 'site_meta_description', 'value' => $this->input->post('site_meta_description')),
                                    // array('name' => 'site_status', 'value' => $this->input->post('site_status')),
                                    array('name' => 'site_page_limit', 'value' => $this->input->post('site_page_limit'))
                                );

                if($this->input->post('site_meta_image') != "")
                {
                    $settings_data = array(
                                        array('name' => 'site_meta_image', 'value' => 'files/'.$this->input->post('site_meta_image'))
                                    );
                }

                $this->settings_model->update_batch($settings_data, 'name');

                $this->session->set_flashdata('success_message','Your settings were successfully saved.');
                redirect('admin/settings');
            }
        }

        // Breadcrumb setting
        $this->load->library('breadcrumb');

        $this->breadcrumb->home_title('<i class="fa fa-home"></i> Home', 'admin');
        $this->breadcrumb->add('Settings');
        // End Breadcrumb setting

        $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);

        $vars = array(
                        'settings'      => $settings,
                        'breadcrumb'    => $breadcrumb
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('Site Settings');
        $this->render('settings_view');
    }

    //--------------------------------------------------------------------
}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */