<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
		Settings
		</h1>
		<?php echo $breadcrumb; ?>
	</section>

	<!-- Main content -->
	<section class="content">

	<?php if($this->session->flashdata('success_message') != "") : ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> Success!</h4>
		<?php echo $this->session->flashdata('success_message'); ?>
	</div>
    <?php endif; ?>

    <?php if($this->session->flashdata('error_message') != "") : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-ban"></i> Error!</h4>
		<?php echo $this->session->flashdata('error_message'); ?>
	</div>
    <?php endif; ?>

		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">Site Information</h3>
					</div><!-- /.box-header -->

					<?php echo form_open();?>

					<?php
						$error_class 	= 'has-error';
						$error_icon 	= '<i class="fa fa-times-circle-o"></i>';
					?>

						<div class="box-body">
							<!-- Site Name -->
							<div class="form-group <?php echo form_error('site_name') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('site_name') ? $error_icon : ''; ?> Site Name*
								</label>
								<input type="text" name="site_name" class="form-control" value="<?php echo $settings['site_name']; ?>" required>
								<p class="help-block"><?php echo form_error('site_name');?></p>
							</div>

							<!-- Company Name -->
							<div class="form-group <?php echo form_error('site_company_name') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('site_company_name') ? $error_icon : ''; ?> Company Name*
								</label>
								<input type="text" name="site_company_name" class="form-control" value="<?php echo $settings['site_company_name']; ?>" required>
								<p class="help-block"><?php echo form_error('site_company_name');?></p>
							</div>

							<!-- Email -->
							<div class="form-group <?php echo form_error('site_email') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('site_email') ? $error_icon : ''; ?> Email*
								</label>
								<input type="email" name="site_email" class="form-control" value="<?php echo $settings['site_email']; ?>" required>
								<p class="help-block"><?php echo form_error('site_email');?></p>
							</div>

							<!-- Address -->
							<div class="form-group <?php echo form_error('site_address') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('site_address') ? $error_icon : ''; ?> Address*
								</label>
								<input type="text" name="site_address" class="form-control" value="<?php echo $settings['site_address']; ?>" required>
								<p class="help-block"><?php echo form_error('site_address');?></p>
							</div>

							<!-- Phone -->
							<div class="form-group <?php echo form_error('site_phone') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('site_phone') ? $error_icon : ''; ?> Phone*
								</label>
								<input type="text" name="site_phone" class="form-control" value="<?php echo $settings['site_phone']; ?>" required>
								<p class="help-block"><?php echo form_error('site_phone');?></p>
							</div>

							<!-- Mobile Phone -->
							<div class="form-group <?php echo form_error('site_mobile_phone') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('site_mobile_phone') ? $error_icon : ''; ?> Mobile Phone*
								</label>
								<input type="text" name="site_mobile_phone" class="form-control" value="<?php echo $settings['site_mobile_phone']; ?>" required>
								<p class="help-block"><?php echo form_error('site_mobile_phone');?></p>
							</div>

							<hr>

							<!-- Facebook Link -->
							<div class="form-group <?php echo form_error('site_facebook_link') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('site_facebook_link') ? $error_icon : ''; ?> Facebook Link
								</label>
								<input type="text" name="site_facebook_link" class="form-control" value="<?php echo $settings['site_facebook_link']; ?>">
								<p class="help-block"><?php echo form_error('site_facebook_link');?></p>
							</div>

							<!-- Twitter Link -->
							<div class="form-group <?php echo form_error('site_twitter_link') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('site_twitter_link') ? $error_icon : ''; ?> Twitter Link
								</label>
								<input type="text" name="site_twitter_link" class="form-control" value="<?php echo $settings['site_twitter_link']; ?>">
								<p class="help-block"><?php echo form_error('site_twitter_link');?></p>
							</div>

							<!-- Instagram Link -->
							<div class="form-group <?php echo form_error('site_instagram_link') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('site_instagram_link') ? $error_icon : ''; ?> Instagram Link
								</label>
								<input type="text" name="site_instagram_link" class="form-control" value="<?php echo $settings['site_instagram_link']; ?>">
								<p class="help-block"><?php echo form_error('site_instagram_link');?></p>
							</div>

							<!-- Google Plus Link -->
							<div class="form-group <?php echo form_error('site_gplus_link') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('site_gplus_link') ? $error_icon : ''; ?> Google Plus Link
								</label>
								<input type="text" name="site_gplus_link" class="form-control" value="<?php echo $settings['site_gplus_link']; ?>">
								<p class="help-block"><?php echo form_error('site_gplus_link');?></p>
							</div>

							<!-- Youtube Link -->
							<div class="form-group <?php echo form_error('site_youtube_link') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('site_youtube_link') ? $error_icon : ''; ?> Youtube Link
								</label>
								<input type="text" name="site_youtube_link" class="form-control" value="<?php echo $settings['site_youtube_link']; ?>">
								<p class="help-block"><?php echo form_error('site_youtube_link');?></p>
							</div>

							<hr>

							<!-- Meta Title -->
							<div class="form-group <?php echo form_error('site_meta_title') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('site_meta_title') ? $error_icon : ''; ?> Meta Title*
								</label>
								<input type="text" name="site_meta_title" class="form-control" value="<?php echo $settings['site_meta_title']; ?>" required>
								<p class="help-block"><?php echo form_error('site_meta_title');?></p>
							</div>

							<!-- Meta Site Name -->
							<div class="form-group <?php echo form_error('site_meta_site_name') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('site_meta_site_name') ? $error_icon : ''; ?> Meta Site Name*
								</label>
								<input type="text" name="site_meta_site_name" class="form-control" value="<?php echo $settings['site_meta_site_name']; ?>" required>
								<p class="help-block"><?php echo form_error('site_meta_site_name');?></p>
							</div>

							<!-- Meta Description -->
							<div class="form-group <?php echo form_error('site_meta_description') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('site_meta_description') ? $error_icon : ''; ?> Meta Description*
								</label>
								<input type="text" name="site_meta_description" class="form-control" value="<?php echo $settings['site_meta_description']; ?>" required>
								<p class="help-block"><?php echo form_error('site_meta_description');?></p>
							</div>

							<!-- Meta Image -->
							<div class="form-group <?php echo form_error('site_meta_image') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('site_meta_image') ? $error_icon : ''; ?> Meta Image*
								</label>
								<?php if(! empty($settings['site_meta_image'])) : ?>
								<img src="<?php echo config_item('uploads_url').$settings['site_meta_image']; ?>" style="display: block; margin: 5px 0 10px 0; width: 300px; height: auto;">
								<?php endif; ?>
								<div class="input-group">
									<div class="input-group-btn">
										<a href="<?php echo base_url().'filemanager/dialog.php?type=2&field_id=site_meta_image_path&relative_url=1'; ?>" class="file-iframe-btn" data-fancybox-type="iframe">
											<button type="button" class="btn btn-block btn-default btn-flat">Browse</button>
										</a>
									</div>
									<input id="site_meta_image_path" name="site_meta_image" type="text" class="form-control">
								</div>
								<p class="help-block"><?php echo form_error('site_meta_image');?></p>
							</div>

							<hr>

							<!-- Site Status -->
							<!--<div class="form-group <?php echo form_error('site_status') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('site_status') ? $error_icon : ''; ?> Site Status*
								</label>
								<select name="site_status" class="form-control">
									<option value="1" <?php echo $settings['site_status'] == "1" ? "selected='selected'" : ""; ?>>
										Online
									</option>
									<option value="0" <?php echo $settings['site_status'] == "0" ? "selected='selected'" : ""; ?>>
										Offline
									</option>
								</select>
								<p class="help-block"><?php echo form_error('site_status');?></p>
							</div>-->

							<!-- Site Page Limit -->
							<div class="form-group <?php echo form_error('site_page_limit') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('site_page_limit') ? $error_icon : ''; ?> Site Page Limit*
								</label>
								<input type="text" name="site_page_limit" class="form-control" value="<?php echo $settings['site_page_limit']; ?>" required>
								<p class="help-block"><?php echo form_error('site_page_limit');?></p>
							</div>
						</div><!-- /.box-body -->

						<div class="box-footer">
							<input type="submit" name="submit" class="btn btn-primary" value="Save Settings">
						</div>
					<?php echo form_close();?>
				</div><!-- /.box -->
			</div><!--/.col (right) -->
		</div>

	</section><!-- /.content -->

</div><!-- /.content-wrapper -->