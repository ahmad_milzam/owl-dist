<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_groups extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();

        if(!$this->ion_auth->in_group('admin'))
        {
            $this->session->set_flashdata('access_error_message','You are not allowed to visit this page');
            redirect('admin');
        }
    }

    //--------------------------------------------------------------------


    public function index()
    {
        $groups = $this->ion_auth->groups()->result();
        if(! empty($groups))
        {
            foreach($groups AS $group)
            {
                $group->total_user = $this->ion_auth->groups()->count_groups_user($group->id);
            }
        }

        // Breadcrumb setting
        $this->load->library('breadcrumb');

        $this->breadcrumb->home_title('<i class="fa fa-home"></i> Home', 'admin');
        $this->breadcrumb->add('Groups');
        // End Breadcrumb setting
        
        $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);

        $vars = array(
                        'groups'        => $groups,
                        'breadcrumb'    => $breadcrumb
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('Groups');
        $this->render('groups_view');
    }

    //--------------------------------------------------------------------


    public function create()
    {
        if ($this->input->post('submit'))
        {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('name','Name','required|trim|max_length[20]|is_unique[groups.name]');
            $this->form_validation->set_rules('description','description','required|trim|max_length[100]');

            $this->form_validation->set_error_delimiters('', '');

            if($this->form_validation->run() === FALSE)
            {
                $this->render('groups_create_view');
            }
            else
            {
                $name         = $this->input->post('name');
                $description  = $this->input->post('description');
                $this->ion_auth->create_group($name, $description);

                $this->session->set_flashdata('success_message',$this->ion_auth->messages());
                redirect('admin/groups');
            }
        }

        // Breadcrumb setting
        $this->load->library('breadcrumb');

        $this->breadcrumb->home_title('<i class="fa fa-home"></i> Home', 'admin');
        $this->breadcrumb->add('Groups', 'admin/groups');
        $this->breadcrumb->add('Create Group');
        // End Breadcrumb setting

        $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);

        $vars = array(
                        'breadcrumb'    => $breadcrumb
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('Groups - Create Group');
        $this->render('groups_create_view');
    }

    //--------------------------------------------------------------------


    public function edit($group_id)
    {
        $check_group = $this->ion_auth->group((int) $group_id)->row();
        if(empty($group_id) || ! $check_group)
        {
            $this->session->set_flashdata('warning_message','Invalid group ID');
            redirect('admin/groups');
        }
        else
        {
            $group[] = $check_group;
        }

        if ($this->input->post('submit'))
        {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('name','Group Name','required|trim|max_length[20]');
            $this->form_validation->set_rules('description','Group description','required|trim|max_length[100]');
            $this->form_validation->set_rules('group_id','Group id','required|trim|integer');

            $this->form_validation->set_error_delimiters('', '');

            if($this->form_validation->run() === FALSE)
            {
                $this->render('groups_edit_view');
            }
            else
            {
                $name         = $this->input->post('name');
                $description  = $this->input->post('description');
                $group_id     = $this->input->post('group_id');
                $this->ion_auth->update_group($group_id, $name, $description);

                $this->session->set_flashdata('success_message',$this->ion_auth->messages());
                redirect('admin/groups/edit/'.$group_id);
            }
        }

        // Breadcrumb setting
        $this->load->library('breadcrumb');

        $this->breadcrumb->home_title('<i class="fa fa-home"></i> Home', 'admin');
        $this->breadcrumb->add('Groups', 'admin/groups');
        $this->breadcrumb->add('Edit Group');
        // End Breadcrumb setting

        $breadcrumb = $this->breadcrumb->output($this->breadcrumb_param);

        $vars = array(
                        'group'         => $group,
                        'breadcrumb'    => $breadcrumb
                    );

        // print_r($vars);exit();

        $this->template->set($vars);
        $this->template->title('Groups - Edit Group');
        $this->render('groups_edit_view');
    }

    //--------------------------------------------------------------------


    public function delete($group_id)
    {
        if(is_null($group_id))
        {
            $this->session->set_flashdata('message','There\'s no group to delete');
        }
        else
        {
            $this->ion_auth->delete_group($group_id);
            $this->session->set_flashdata('success_message',$this->ion_auth->messages());
        }

        redirect('admin/groups');
    }

    //--------------------------------------------------------------------
}