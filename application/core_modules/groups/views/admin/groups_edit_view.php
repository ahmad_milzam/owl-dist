<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
		Groups
		</h1>
		<?php echo $breadcrumb; ?>
	</section>

	<!-- Main content -->
	<section class="content">

	<?php if($this->session->flashdata('success_message') != "") : ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> Success!</h4>
		<?php echo $this->session->flashdata('success_message'); ?>
	</div>
    <?php endif; ?>

		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">Edit Group</h3>
					</div><!-- /.box-header -->

					<?php echo form_open();?>

					<?php
						$error_class 	= 'has-error';
						$error_icon 	= '<i class="fa fa-times-circle-o"></i>';
					?>

						<div class="box-body">
							<!-- Name -->
							<div class="form-group <?php echo form_error('name') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('name') ? $error_icon : ''; ?> Group Name*
								</label>
								<input type="text" name="name" class="form-control" value="<?php echo $group[0]->name; ?>" required>
								<p class="help-block"><?php echo form_error('name');?></p>
							</div>

							<!-- Description -->
							<div class="form-group <?php echo form_error('description') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('description') ? $error_icon : ''; ?>Group Description*
								</label>
								<textarea name="description" class="form-control" rows="3" required><?php echo $group[0]->description; ?></textarea>
								<p class="help-block"><?php echo form_error('description');?></p>
							</div>

							<input type="hidden" name="group_id" value="<?php echo $group[0]->id; ?>">
						</div><!-- /.box-body -->

						<div class="box-footer">
							<input type="submit" name="submit" class="btn btn-primary" value="Edit Group">
							&nbsp;
							<a href="<?php echo base_url('admin/groups'); ?>" class="btn btn-default">
								Cancel
							</a>

							<?php if($this->ion_auth->is_admin() && ! in_array($group[0]->name, array('admin'))) : ?>
							&nbsp;|&nbsp;
							<a href="<?php echo base_url().'admin/groups/delete/'.$group[0]->id; ?>" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this group?')">
								<i class="fa fa-fw fa-trash-o"></i> Delete
							</a>
							<?php endif; ?>
						</div>
					<?php echo form_close();?>
				</div><!-- /.box -->
			</div><!--/.col (right) -->
		</div>

	</section><!-- /.content -->

</div><!-- /.content-wrapper -->