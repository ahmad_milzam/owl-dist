<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
		Groups
		</h1>
		<?php echo $breadcrumb; ?>
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">Create New Group</h3>
					</div><!-- /.box-header -->

					<?php echo form_open();?>

					<?php
						$error_class 	= 'has-error';
						$error_icon 	= '<i class="fa fa-times-circle-o"></i>';
					?>

						<div class="box-body">
							<!-- Name -->
							<div class="form-group <?php echo form_error('name') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('name') ? $error_icon : ''; ?> Group Name*
								</label>
								<input type="text" name="name" class="form-control" required>
								<p class="help-block"><?php echo form_error('name');?></p>
							</div>

							<!-- Description -->
							<div class="form-group <?php echo form_error('description') ? $error_class : ''; ?>">
								<label>
									<?php echo form_error('description') ? $error_icon : ''; ?>Group Description*
								</label>
								<textarea name="description" class="form-control" rows="3" required></textarea>
								<p class="help-block"><?php echo form_error('description');?></p>
							</div>
						</div><!-- /.box-body -->

						<div class="box-footer">
							<input type="submit" name="submit" class="btn btn-primary" value="Create Group">
							&nbsp;
							<a href="<?php echo base_url('admin/groups'); ?>" class="btn btn-default">
								Cancel
							</a>
						</div>
					<?php echo form_close();?>
				</div><!-- /.box -->
			</div><!--/.col (right) -->
		</div>

	</section><!-- /.content -->

</div><!-- /.content-wrapper -->