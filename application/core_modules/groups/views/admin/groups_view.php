<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
		Groups
		</h1>
		<?php echo $breadcrumb; ?>
	</section>

	<!-- Main content -->
	<section class="content">

	<?php if($this->session->flashdata('success_message') != "") : ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> Success!</h4>
		<?php echo $this->session->flashdata('success_message'); ?>
	</div>
    <?php endif; ?>

	<?php if($this->session->flashdata('warning_message') != "") : ?>
	<div class="alert alert-warning alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-warning"></i> Warning!</h4>
		<?php echo $this->session->flashdata('warning_message'); ?>
	</div>
    <?php endif; ?>

	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Group List</h3>
					<div class="box-tools">
						<a href="<?php echo base_url('admin/groups/create'); ?>">
							<button class="btn btn-block btn-primary btn-sm">Create Group</button>
						</a>
					</div>
				</div><!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tbody>
							<tr>
								<th>No.</th>
								<th>Name</th>
								<th>Total Users</th>
								<th>Description</th>
							</tr>
						<?php if(! empty($groups)) : ?>
							<?php $i=0; foreach($groups AS $group) : $i++; ?>
							<tr>
								<td><?php echo $i; ?></td>
								<td>
									<a href="<?php echo base_url().'admin/groups/edit/'.$group->id; ?>">
										<?php echo $group->name; ?>
									</a>
								</td>
								<td><?php echo $group->total_user; ?></td>
								<td><?php echo $group->description; ?></td>
							</tr>
							<?php endforeach; ?>
						<?php else : ?>
							<tr>
								<td colspan="4">Empty records..</td>
							</tr>
						<?php endif; ?>
						</tbody>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>

	</section><!-- /.content -->

</div><!-- /.content-wrapper -->