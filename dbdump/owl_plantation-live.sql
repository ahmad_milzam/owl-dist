-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 02, 2016 at 02:30 AM
-- Server version: 5.5.48-cll
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lomba_owl`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `sub_title` varchar(255) DEFAULT NULL,
  `image_large` varchar(255) DEFAULT NULL,
  `image_small` varchar(255) DEFAULT NULL,
  `image_alt_text` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '99',
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `title`, `sub_title`, `image_large`, `image_small`, `image_alt_text`, `url`, `slug`, `position`, `publish`, `deleted`, `created_on`, `modified_on`) VALUES
(1, 'Banner 1', NULL, 'files/banners/large/banner1.jpg', 'files/banners/small/banner1.jpg', 'Banner 1', '', 'banner-1', 1, 1, 1, '2016-05-10 16:52:53', '2016-05-13 16:35:34'),
(2, 'Banner 2', NULL, 'files/banners/large/banner2.jpg', 'files/banners/small/banner2.jpg', 'Banner 2', '', 'banner-2', 2, 1, 1, '2016-05-13 15:50:28', '2016-05-13 16:17:03'),
(3, 'Banner 3', NULL, 'files/banners/large/banner3.jpg', 'files/banners/small/banner3.jpg', 'Banner 3', '', 'banner-3', 3, 1, 1, '2016-05-13 15:52:15', '2016-05-13 16:17:15'),
(4, 'We Grow Your Bussiness', 'Trustworthy ERP Plantation Business Solution', 'files/banners/large/banner1.jpg', 'files/banners/small/banner1.jpg', 'Banner alt tag', 'http://lombablog.com/', 'we-grow-your-bussiness', 1, 1, 0, '2016-05-21 00:14:34', '0000-00-00 00:00:00'),
(5, 'Test', '', 'files/banners/large/banner1.jpg', 'files/banners/small/banner1.jpg', 'Test', '', 'test', 1, 1, 1, '2016-05-26 23:34:17', '0000-00-00 00:00:00'),
(6, 'Emas Hijau Penyelamay Hidup Petani', 'Emas Hijau Penyelamay Hidup Petani', 'files/banners/large/View-lembah-dari-kebun-kelapa-sawit-Copy.jpg', 'files/banners/small/areal-kebun-sawit-mmj-dituding-bermasalah-.jpg', 'SAWIT INDONESIA', 'www.owl-plantation.com', 'emas-hijau-penyelamay-hidup-petani', 2, 1, 0, '2016-05-27 00:04:01', '2016-05-27 00:20:18');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `phone`, `message`, `deleted`, `created_on`, `modified_on`) VALUES
(1, 'Andhika', 'andhikanovandi@gmail.com', '12345678', 'asdasdasd', 0, '2016-05-16 13:53:27', '0000-00-00 00:00:00'),
(2, 'Andhika', 'andhikanovandi@gmail.com', '12345678', 'asdasdasd', 0, '2016-05-16 13:53:40', '0000-00-00 00:00:00'),
(3, 'Andhika', 'andhikanovandi@gmail.com', '123', 'asdasdasd', 0, '2016-05-16 14:02:16', '0000-00-00 00:00:00'),
(4, 'test', 'test@test.com', '000000', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test', 0, '2016-05-24 03:40:55', '0000-00-00 00:00:00'),
(5, 'Erica', 'ericadwisaraswati@gmail.com', '085714072662', 'Test email contact di web.\r\nMohon dibalas..', 0, '2016-05-26 23:06:07', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `title`, `date`, `file`, `slug`, `publish`, `deleted`, `created_on`, `modified_on`) VALUES
(1, 'File 1', '2016-05-10', 'files/sample.pdf', 'file-1', 1, 0, '2016-05-08 01:50:01', '2016-05-14 23:31:30'),
(2, 'OWL BI', '2016-05-20', 'files/BI_Graph.jpg', 'owl-bi', 1, 0, '2016-05-19 22:37:06', '0000-00-00 00:00:00'),
(3, 'File Excel', '2016-05-27', 'files/log_5saldobulanan (1).csv', 'file-excel', 1, 0, '2016-05-27 00:11:30', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'member', 'General User'),
(3, 'editor', 'Editor');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `content` text NOT NULL,
  `date` date NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `view_count` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `summary`, `content`, `date`, `image`, `slug`, `publish`, `view_count`, `deleted`, `created_on`, `modified_on`) VALUES
(1, 'News 1', 'News 1. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae. Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', '<p>News 1. Cras ultricies ligula sed magna dictum porta. Vivamus suscipit tortor eget felis porttitor volutpat. Vivamus suscipit tortor eget felis porttitor volutpat. Nulla porttitor accumsan tincidunt. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n\r\n<p>Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tincidunt. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>', '2016-05-09', 'files/lipsum.jpg', 'news-1', 1, 15, 0, '2016-05-05 21:56:41', '2016-05-30 06:45:55'),
(2, 'News 2', 'News 2. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae. Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', '<p>News 2. Cras ultricies ligula sed magna dictum porta. Vivamus suscipit tortor eget felis porttitor volutpat. Vivamus suscipit tortor eget felis porttitor volutpat. Nulla porttitor accumsan tincidunt. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n\r\n<p>Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tincidunt. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>', '2016-05-10', 'files/lipsum.jpg', 'news-2', 1, 8, 0, '2016-05-06 21:56:41', '2016-05-30 06:45:49'),
(3, 'News 3', 'News 3. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae. Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', '<p>News 3. Cras ultricies ligula sed magna dictum porta. Vivamus suscipit tortor eget felis porttitor volutpat. Vivamus suscipit tortor eget felis porttitor volutpat. Nulla porttitor accumsan tincidunt. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n\r\n<p>Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tincidunt. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>', '2016-05-11', 'files/lipsum.jpg', 'news-3', 1, 14, 0, '2016-05-07 21:56:41', '2016-06-01 01:15:26'),
(4, 'NEWS 4', 'News 3. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae. Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', '<ol>\r\n	<li>\r\n	<hr />News 3. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae. Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>News 3. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae. Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>News 3. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae. Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>News 3. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae. Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>News 3. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae. Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>News 3. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae. Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>', '2016-05-20', 'files/lipsum.jpg', 'news-4', 1, 12, 0, '2016-05-19 22:42:11', '2016-05-30 06:45:45');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `name` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`name`, `value`) VALUES
('site_address', 'Jl. Wijaya VII No. 1, Kebayoran Baru, Jakarta Selatan'),
('site_company_name', 'PT.ORIGIN WIRACIPTA LESTARI'),
('site_email', 'nangkoel@owl-plantation.com'),
('site_facebook_link', 'https://facebook.com'),
('site_gplus_link', 'https://plus.google.com'),
('site_instagram_link', 'https://instagram.com'),
('site_meta_description', 'owl Plantation'),
('site_meta_image', 'files/lipsum.jpg'),
('site_meta_site_name', 'owl-plantation.com'),
('site_meta_title', 'OWL - Plantation'),
('site_mobile_phone', '+62 81311351132'),
('site_name', 'OWL - Plantation'),
('site_page_limit', '25'),
('site_phone', '+62 2172780146'),
('site_status', '1'),
('site_twitter_link', 'https://twitter.com'),
('site_youtube_link', 'https://youtube.com');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE IF NOT EXISTS `testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `video_url` varchar(255) DEFAULT NULL,
  `video_id` varchar(255) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `title`, `video_url`, `video_id`, `slug`, `publish`, `deleted`, `created_on`, `modified_on`) VALUES
(1, 'Testimonial 1', 'https://www.youtube.com/watch?v=4Nv4YcrtTl8', '4Nv4YcrtTl8', 'testimonial-1', 1, 0, '2016-05-07 23:57:30', '2016-05-14 22:20:52'),
(2, 'Testimonial 2', 'https://www.youtube.com/watch?v=YUJ-nU9YOQE', 'YUJ-nU9YOQE', 'testimonial-2', 1, 0, '2016-05-14 22:22:08', '0000-00-00 00:00:00'),
(3, 'Testimonial 3', 'https://www.youtube.com/watch?v=sqfmHMAr1Oc', 'sqfmHMAr1Oc', 'testimonial-3', 1, 0, '2016-05-14 22:23:55', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'owladmin', '$2y$08$Vod279pZsCXb5MNlY7zSYOM1t9jf1Z8G9BPykEy8g/IoMesNA1kE.', '', 'owladmin@gmail.com', '', NULL, NULL, 'LwNNJCQXsVBJkIh13wZ/Au', 1268889823, 1464664792, 1, 'Owl', 'Admin', '', ''),
(3, '202.137.25.130', 'owleditor', '$2y$08$rg1jQRFF19w9PEn75JeVCu8gQo5ydPVR2mouczYJD8x4c/HB2sajC', NULL, 'owleditor@gmail.com', NULL, NULL, NULL, NULL, 1464152527, NULL, 1, 'Owl', 'Editor', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(9, 1, 1),
(10, 3, 3);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
