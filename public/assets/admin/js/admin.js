$(function () {
    // Replace the <textarea class="textarea-ckeditor"> with a CKEditor instance, using custom configuration.
    $(".wysiwyg-simple").each( function() {
        CKEDITOR.replace( $(this).attr('id'), {
            toolbar : 'Basic',
            height  : 200
        });
    });
    $(".wysiwyg-advanced").each( function() {
        CKEDITOR.replace( $(this).attr('id'), {
            toolbar : 'Standard',
            height  : 250
        });
    });

    $(".wysiwyg-simple-br").each( function() {
        CKEDITOR.replace( $(this).attr('id'), {
            customConfig : BASE_URL + 'assets/admin/js/ckeditor_config.js',
            toolbar : 'Basic',
            height  : 200
        });
    });
    $(".wysiwyg-advanced-br").each( function() {
        CKEDITOR.replace( $(this).attr('id'), {
            customConfig : BASE_URL + 'assets/admin/js/ckeditor_config.js',
            toolbar : 'Standard',
            height  : 250
        });
    });
    //-------------------------


    // File Manager Button
    $('.file-iframe-btn').fancybox({
        autoSize      : false
    });
    //-------------------------


    // Datepicker search form
    $('#form-date .input-daterange').datepicker({
        format: "dd-mm-yyyy",
        autoclose: true,
        todayHighlight: true
    });
    //-------------------------


    // Datepicker create form
    $('.input-datepicker').datepicker({
        format: "dd-mm-yyyy",
        autoclose: true,
        todayHighlight: true
    });
    //-------------------------
});

function searchForm(dropdown) {
    var value = dropdown.options[dropdown.selectedIndex].value;
    if (value == 'date'){
        document.getElementById("form-container").style.display = "block";
        document.getElementById("form-date").style.display = "block";
        document.getElementById("form-source").style.display = "none";
    }
    if(value == 'source'){
        document.getElementById("form-container").style.display = "block";
        document.getElementById("form-date").style.display = "none";
        document.getElementById("form-source").style.display = "block";
    }
    if(value == ''){
        // document.getElementById("form-container").style.display = "none";
        document.getElementById("form-date").style.display = "none";
        document.getElementById("form-source").style.display = "none";
    }
}