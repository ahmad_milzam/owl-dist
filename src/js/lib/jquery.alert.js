// UTIL FUNCTION
/* From Modernizr */
(function($){

  /* Listen for a transition! */
  var transitionEvent = whichTransitionEnd();
  var animationEvent = whichAnimationEnd();

  // ALERTBOX HANDLER
  $(document).off('.js-alert').on('click.alert', '.js-alert-close', function (e) {
    e.preventDefault();

    var $alertBox = $(this).closest('.js-alert');

    if($alertBox.hasClass('alert-in')){
      $alertBox.removeClass('alert-in');
    }

    if(animationEvent){
      console.log('alert out with animation');

      $alertBox.addClass('alert-out');
      $alertBox.on(animationEvent, function (e) {
        $(this).trigger('close.alert').remove();
      });

    } else if (animationEvent == false && transitionEvent) {

      console.log('alert out with transition');

      $alertBox.addClass('alert-out');
      $alertBox.on(transitionEvent, function (e) {
        $(this).trigger('close.alert').remove();
      });

    } else {

      $alertBox.fadeOut(300, function () {
        $(this).trigger('close.alert').remove();
      });

    }
  });

})(jQuery);
