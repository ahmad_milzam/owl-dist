;(function($) {
  'use strict';

  console.log('app run');
  console.log('gulp build system works!');

  $(function() {

    /*----------  main navbar  ----------*/
    var $menuList = $('#js-navList'),
        $menuToggle = $('#js-navToggle');

    $menuToggle.on('click',function() {
      $menuList.toggleClass('is-active');
      $menuToggle.toggleClass('is-active');
      return false;
    });


    /*----------  slider  ----------*/
    $('#js-slide').slick({
      autoplay: true,
      autoplaySpeed: 3000,
      dots: true,
      arrows: false,
      // prevArrow: '.slide__nav--prev',
      // nextArrow: '.slide__nav--next',
      infinite: true,
      speed: 1000,
      fade: true,
      cssEase: 'ease'
    });

    /*----------  form  ----------*/
    var $form = $('#js-form'),
        $submitBtn = $form.find('[type=submit]'),
        loadingState = 'Please Wait..',
        defaultState = $submitBtn.children().html(),
        $feedback = $('#js-feedback');

    $form.on('submit', function(event) {
      event.preventDefault();

      $submitBtn.children().html(loadingState);

      $.ajax({
        url: $form.attr('action'),
        type: $form.attr('method'),
        dataType: 'json',
        data: $form.serialize(),
      })
      .done(function(response, textStatus, jqXHR){
        if(response.success == 1){
          // success
          AlertBox.show('Thank you, your message has been sent.', {
            alertType: 'success'
          });
          $form.clearForm();
        } else{
          // failed
          AlertBox.show('<ul class="alert__list">'+response.error+'</ul>', {
            alertType: 'error'
          });
        }
      })
      .fail(function( jqXHR, textStatus, errorThrown ){
        console.log('An error occured\n'+errorThrown);
      })
      .always(function(){
        $submitBtn.children().html(defaultState);
      });

      return false;
    });

  });

}(jQuery));